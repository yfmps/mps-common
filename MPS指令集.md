# MPS指令集

## 修改记录

- v2.3.0 蔡平 2020/5 初始版本编写
- v2.3.1 龙武全 2021/4/25 删除**名称**行，标题中已经有了删除**返回类型**行，目前只有同步类型删除指令汇总表所有示例，都可以Copy运行添加超链接，并调整目录结构对照指令源代码，完善手册添加全局变量表于指令开始处，如[任务全局变量](#任务全局变量)
- v2.3.2 龙武全 2021/4/28 更新指令数据结构
- V2.3.3 龙武全 2021/4/30 添加指令 util.HttpPost、util.HttpGet
- V2.3.4 龙武全 2021/5/18 添加指令task.GetTaskTime、task.UpdateTaskTime、task.GetBindAgvTime、task.SetBindAgvTime、task.GetDeliveryTime、task.SetDeliveryTime更新task.Task、task.TaskFilter
- v2.3.5 龙武全 2021/9/14
  - 基于V2.3.4版本进行修改，改为[Markdown](http://www.markdown.cn/)格式（使用[Typora](https://typora.io/)编辑）
  - 添加[云平台指令](#cloud-云平台)，[MD5](#md5-计算md5)，[车牌识别指令](#lpr-车牌识别指令)
  - 添加示例，[工具脚本](#工具脚本)，[脚本约定功能](#脚本约定功能)，[服务地址](#服务地址)，[REST Client脚本](#rest-client-脚本)，[在线联调脚本](#在线联调脚本)
  - 指令类型按字母排序，成对使用功能指令合并一起

## 概述

MPS指令集用于MPS系统的业务流程定义，使用Lua作为脚本语言。

指令从设置和获取的角度，可分为两类，一类是设置、写入数据指令，一类是获取数据指令，对于指令的返回值，我们规定，所有设置类型的指令，只返回一个值，该值为一个错误对象，通过该对象可检查指令是否执行成功；所有获取类型的指令，均返回两个返回值，第一个值为获取的数据，第二个值为错误对象，作用同设置类型指令。

调用获取类型指令后，必须检查错误对象返回正确与否，再决定是否使用获取的数据，在错误对象返回错误的情况下，获取的数据为无效数据。

## 错误状态对象

### Status 错误状态定义

```go
type Status struct {
  code ErrorCode
  msg  string
}
```

### Status 方法

系统内置了status模块，该模块具有两个方法，用于在lua脚本内创建Status错误对象。

#### 模块方法

| 模块方法              | 参数类型           | 返回值 | 说明                                 |
| --------------------- | ------------------ | ------ | ------------------------------------ |
| status.new(code, msg) | code intmsg string | status | 根据错误码和错误信息，新建Status对象 |
| status.OK()           | -                  | status | 新建一个无错误的Status对象           |

#### 对象方法

| 对象方法          | 参数类型 | 返回值 | 说明                                                      |
| ----------------- | -------- | ------ | --------------------------------------------------------- |
| status:IsOK()     | -        | bool   | 检查指令执行是否成功,错误码为0，即为成功                  |
| status:IsFailed() | -        | bool   | 检查指令执行是否失败，错误码不为0，即为失败               |
| status:Code()     | -        | int    | 返回错误码，详情将《[错误码定义](#status-标准错误码定义)》 |
| status:Message()  | -        | string | 返回错误描述信息                                          |
| status:String()   | -        | string | 返回错误码和错误描述信息的字符串                          |

**注意**：lua中在对象上调用方法时，使用冒号(:)而不是句号(.)，当然在模块上仍使用 . 点调用模块的方法。

### status 标准错误码定义

status内置错误码信息，用于Status对象，可直接在脚本中使用，错误码是一个整数类型，当前定义的错误码如下：

| 错误码                     | 值            | 含义                               |
| -------------------------- | ------------- | ---------------------------------- |
| status.NoError             | 0             | 表示没有错误                       |
| status.ErrRPC              | 1             | 服务调用出错                       |
| status.ErrParameterInvalid | 2             | 参数无效                           |
| status.ErrNotExist         | 3             | 数据不存在                         |
| status.ErrPropertyInvalid  | 4             | 属性无效                           |
| status.ErrIDDuplicate      | 5             | ID重复或者ID已存在                 |
| status.ErrIDNotExist       | 6             | ID不存在                           |
| status.ErrWriteFile        | 7             | 写文件出错                         |
| status.ErrDatabase         | 8             | 数据库操作错误                     |
| status.ErrLock             | 9             | 锁错误                             |
| status.ErrNotConnected     | 10            | 第三方系统未建立连接，比如调度系统 |
| status.ErrSyntax           | 11            | 脚本语法错误                       |
| status.ErrOccupied         | 12            | 资源已被占用                       |
| status.ErrUnknown          | math.MaxInt32 | 未知错误                           |

在使用时，推荐使用上述定义的错误码形式，比如出错时检查错误是否为数据不存在，则使用err:Code() == status.ErrNotExist，而不是使用err:Code() == 3，因为对应错误码的数字有可能在系统更新迭代中变化。

### status 使用参考

当需要在lua中创建Status对象时，如

```lua
sts = status.new(status.ErrParameterInvalid, "参数未定义")

--  创建了一个名为sts的Status对象，
if sts:IsOK() == false then
  print("执行失败")
end

if sts:IsFalied() == true then
  print("执行失败")
end

--  打印错误码及错误信息
print(sts:String())
```

## 脚本参数输入

当一个脚本被创建时，传递给脚本的参数以键值对的方式被保存在Parameters的字典对象中。

比如，我们需要启动一个任务脚本test.lua，并传入entranceID为1、materialID为100的两个参数，则在test.lua中，以如下方式将参数取回：

```lua
entranceID = Parameters["entranceID"]
materialID = Parameters["materialID"]
```

并且其类型与传入类型一致，脚本中按正常操作即可，不需要额外进行类型转换。但为了兼容界面启动任务，建议在脚本中对输入参数进行显示转换。

## 启动脚本的两种方式

有两种方式可以启动一个任务，从而执行脚本，一种是从MPS的图形化系统中启动，一种是脚本内调用task.NewTask指令启动。

从脚本启动时，不用太在意参数类型，按照实际类型之间传入即可；
从界面启动时，需要为输入参数指定参数类型，默认类型为int，脚本在接收参数时，会根据所需类型进行转换；

因此在脚本中接收时，不再需要在脚本中对输入参数进行转换。

## 约定

### 关于任务编号的使用

当系统生成脚本时，会自动将任务编号以TaskID为名称的变量添加到脚本的全局参数，在脚本中可以直接使用，正常情况下，不要给TaskID赋值，只使用即可。

但系统处理s消息时，会根据调度系统的任务编号，找到关联的MPS任务编号，并将MPS任务编号以TaskID为名称的变量添加到s消息处理脚本的全局参数，如果没有找到关联任务，则TaskID为0。

### 数组的表示

在lua中，数组的**索引是从1开始**的，即第一个变量的索引为1，而不是其他语言通常的0。

lua中没有专门的数组类型，是使用table或者{}来表示的，为了后续表示方便，我们使用 [] 表示数组，后续描述中使用type[]，来描述类型为type的数组。

### 数据结构

所有的数据结构，使用GoLang的表示方式，需要特别注意，**lua是区分大小写的**，所有字段的名字要与数据结构中定义的一致。

### 查询语法说明

以任务的查询为例，任务有很多查询项，其中包括阶段Phase、任务号TaskID、开始时间Begin、结束时间End等，详情见[任务指令](#task-任务)，对于这些字段，需要不同的查询方式，比如获取指定任务号的任务，获取阶段小于某个阶段的任务，大于某个阶段的任务，位于某个时间段内的任务等，总体上分为查询某个属性、查询某个范围内的属性值、不在某个范围内的属性值、小于某个属性值、小于等于某个属性值、大于某个属性值、大于等于某个属性值等，为了更统一定义查询方式，对查询变量的命名如下：

以任务的阶段Phase为例：

- 查询某个属性, 则定义Phase
- 查询某个范围内的属性值，则定义PhaseIn
- 不在某个范围内的属性值，则定义PhaseNotIn
- 小于某个属性值，则定义PhaseLT
- 小于等于某个属性值，则定义PhaseLE
- 大于某个属性值，则定义PhaseGT
- 大于等于某个属性值，则定义PhaseGE

其他属性如果要定义类似的语义，按照此规则进行。

**所有Filter的字段名字，是不区分大小写的**，建议与本手册保持一致。

## 指令

### area 区域

#### 区域数据类型

##### Area 区域信息

指令结构Area，结合[areaWrapper.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/area/areaWrapper.go) DecodeArea() 与areaPb.Area

```go
type Area struct {
  Id            uint32    // 区域ID
  IsValid       bool      // 是否有效
  IsInbound     bool      // 是否允许入库
  IsOutbound    bool      // 是否允许出库
  OrderCapacity uint32    // 区域任务容量
  ParentAreaID  uint32    // 父级区域ID
  Racklots      []uint32  // 区域的货位列表
  Properties    string    // 区域属性
}
```

##### AreaFilter 区域过滤器

指令结构 AreaFilter，参考[areaFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/area/areaFilter.go) AreaFilter

```go
type AreaFilter struct {
  And         []*AreaFilter     // 过滤器 And
  Or          []*AreaFilter     // 过滤器 Or
  IDIn        []uint32          // ID包括
  ParentID    *uint32           // 父区域ID
  ChildIDIn   []uint32          // 子区域ID包括
  InBound     *bool             // 是否可以入库
  OutBound    *bool             // 是否可以出库
  IsValid     *bool             // 是否有效
  RacklotIDIn []uint32          // 货位ID包括 
  Property    []*PropertyFilter // 属性 
  TypeName    *string           // 类型名称
}
```

##### PropertyFilter 区域过滤器属性

指令结构 PropertyFilter，参考[areaFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/area/areaFilter.go) PropertyFilter

```go
type PropertyFilter struct {
  Key   string      // 属性名
  Value interface{} // 属性值
}
```

#### 区域指令

##### GetArea 获取指定区域

```lua
area.GetArea(areaID)
```

参数

- areaID  uint32  区域编号

返回值

- [Area](#area-区域信息)  成功时为区域信息；失败时为nil

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local areaInfo, err = area.GetArea(1)
log.Infof("IsInbound=%d", areaInfo.IsInbound)
```

##### GetAreas 根据条件查询区域

```lua
area.GetAreas(filter)
```

根据查询条件，查询满足条件的所有任务

参数

- filter  [AreaFilter](#areafilter-区域过滤器)  区域过滤器

返回值

- [Area](#area-区域信息)[]  区域数组

- [Status](#status-标准错误码定义) 错误状态

##### IsExist 区域是否存在

```lua
area.IsExist(areaID)
```

根据区域ID，检查某个区域是否存在

参数

- areaID  int  区域编号

返回值

- bool  区域是否存在

- [Status](#status-标准错误码定义) 错误状态

##### IsAvailable 区域是否可用

```lua
area.IsAvailable(areaID)
```

根据区域ID，检查某个区域是否可用，其可用条件是：

- 区域存在
- 没有被禁用
- 允许入库
- 允许出库

参数

- areaID  int  区域编号

返回值

- bool  区域是否可用

- [Status](#status-标准错误码定义) 错误状态

##### Enable/Disable 启用/禁用区域

```lua
area.Enable(areaID)   -- 启用区域
area.Disable(areaID)  -- 禁用区域
```

参数

- areaID  int  区域编号

返回值

- [Status](#status-标准错误码定义) 错误状态

##### SetCapacity 设置区域任务容量

```lua
area.SetCapacity(areaID, cap)
```

参数

- areaID  int  区域编号
- cap  int  任务容量

返回值

- [Status](#status-标准错误码定义) 错误状态

##### SetOutbound/SetInbound 设置是否允许出/入库

```lua
area.SetInbound(areaID, flag)   -- 设置是否允许入库
area.SetOutbound(areaID, flag)  -- 设置是否允许出库
```

参数

- areaID  int  区域编号
- flag  bool  是否允许出库

返回值

- [Status](#status-标准错误码定义) 错误状态

##### SetProperty 设置区域属性

```lua
area.SetProperty(areaID, arg, argv)
```

使用指令设置某个参数前，该参数必须在该区域类型中进行了定义，如果未定义则不允许设置，需出错返回提示。

参数

- areaID  int  区域编号
- arg  string  属性名称
- argv  string|number|bool  属性值，支持lua的三种类型，分别为字符串、数字(包括整型和浮点型)，布尔类型

返回值

- [Status](#status-标准错误码定义) 错误状态

##### GetProperty 获取区域属性

```lua
area.GetProperty(areaID, arg)
```

获取使用area.SetProperty(areaID, arg, argv)设置的区域属性

参数

- areaID  int  区域编号
- arg  string  属性名称

返回值

- [Status](#status-标准错误码定义) 错误状态

### camera 摄像头

#### camera 指令

##### SnapPicture 抓拍

```lua
camera.SnapPicture(cameraID, channel, tagName)
```

目前相机指令，只适配了**大华相机**[SDK](https://sitefilecdn.dahuatech.com/2.8.01.01.01321/20200525/1119443_General_NetSDK_Chn_Linux64_IS_V3.052.0000001.1.R.200514.tar.gz)，照片后，会在当前程序运行目录下按照如下规则生成图片：

- 按照相机编号，分别为每个相机在./pictures下生成camera_xx形式的目录，xx表示相机编号；
- 每个相机，按照日期在每个相机目录camera_xx下生成日期目录，保存当天的照片，格式为：2020-06-05
- 照片名称为拍照时间+标签名称tagName，格式为：20200605-091011-${tagName}，如果指令未给tagName,则其值默认为相机通道channel的值。

参数

- cameraID  int  相机编号
- channel  int  相机通道
- tagName  string  可选参数，如果给定该参数，会将其作为照片名称的一部分。

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
camera.SnapPicture(1, 1, "1号出入口")
-- 保存的照片为：
-- ./pictures/camera_1/2020-06-05/20200605-091011-1号出入口.jpg

camera.SnapPicture(2, 3)
-- 保存的照片为：
-- ./pictures/camera_2/2020-06-05/20200605-091215-3.jpg
```

##### Config 配置相机参数

```lua
camera.Config(cameraID, channel, ip, port)
```

参数

- cameraID  int  相机编号
- channel  int  相机通道
- ip  string  IP地址
- port  int  端口

返回值

- [Status](#status-标准错误码定义) 错误状态

### cloud 云平台

原 payment 指令，已更新为 cloud，以支持云平台功能对接。

参考资料

- 《[怡丰收费平台与设备系统对接协议_V1.2.1.doc](https://codeup.aliyun.com/yfrobots/mps/MPS-documents/blob/8855d0b0086dbbdf2311d23c80fa27660e872d1e/%E9%80%9A%E8%AE%AF%E5%8D%8F%E8%AE%AE/%E6%80%A1%E4%B8%B0%E6%94%B6%E8%B4%B9%E5%B9%B3%E5%8F%B0%E4%B8%8E%E8%AE%BE%E5%A4%87%E7%B3%BB%E7%BB%9F%E5%AF%B9%E6%8E%A5%E5%8D%8F%E8%AE%AE_V1.2.1.doc)》
- 《[平台和车库终端的报文（以此为主）.xlsx](https://codeup.aliyun.com/yfrobots/mps/MPS-documents/blob/8855d0b0086dbbdf2311d23c80fa27660e872d1e/%E9%80%9A%E8%AE%AF%E5%8D%8F%E8%AE%AE/%E5%B9%B3%E5%8F%B0%E5%92%8C%E8%BD%A6%E5%BA%93%E7%BB%88%E7%AB%AF%E7%9A%84%E6%8A%A5%E6%96%87%EF%BC%88%E4%BB%A5%E6%AD%A4%E4%B8%BA%E4%B8%BB%EF%BC%89.xlsx)》

#### 云平台数据类型

##### CloudPlatform 云平台配置信息

指令结构CloudPlatform，结合[cloudFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/cloud/cloudFilter.go) DecodeCloudPlatformConfig()与cloudPb.CloudPlatform

```go
type CloudPlatform struct {
  Id int32              // ID
  Name string           // 平台名称
  PlatformID uint32     // 车库云平台ID
  PlatformType string   // 平台类型
  PlatformTerminalID string// 车库云平台终端ID
  GarageID uint32       // 车库编号
  OfferType uint32      // 优惠类型
  OfferStrength uint32  // 优惠份额
  PlatformUser string   // 平台用户
  Password string       // 用户密码
  IP string             // 设备IP
  TCPPort int32         // Tcp服务端口
  HTTPHost string       // Http服务地址（如：http://yfweb.yfparking.com:8088/）
  Private string        // 密钥
  ConnectStatus bool    // 连接状态
  Description string    // 设备描述
}
```

##### MonthlyRental 月租卡用户信息

指令结构MonthlyRental，结合[cloudFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/cloud/cloudFilter.go) DecodeMonthlyRental()与cloudPb.MonthlyRental

```go
type MonthlyRental struct {
  Id         int32    // id
  Lpn        string   // 车牌号
  User       string   // 用户名
  Contact    string   // 联系方式
  TimeBegin  int64    // 开始时间
  TimeEnd    int64    // 结束时间
  TimeLength string   // 月租时长
}
```

##### MonthlyRentalFilter 月租用户过滤器

```go
type MonthlyRentalFilter struct {
  And        []*MonthlyRentalFilter
  Or         []*MonthlyRentalFilter
  IDIn       []uint32
  UserIn     []string
  LpnIn      []string
  Timelength []string
}
```

#### 云平台通知

云平台通知，会直接转发到 adv.lua 的 Operation 函数，云平台通知的[操作码](#操作码)定义如下

```lua
adv.Operation(entID, code, tag)
```

参数

- entID   int  出入口编号；0表示没有给编号

- Code  int  操作码

- ParkTag  string  为云平台对应通知的JSON字符串，原样转发给lua

返回值

- string    json 字符串

#### 云平台指令

##### GetCloudPlatformConfig 获取云平台配置

```lua
cloud.GetCloudPlatformConfig(id)
```

参数

- id  int   平台配置ID。id==0 时，获取默认配置；id >0 时，获取指定配置

返回值

- [CloudPlatform](#cloudplatform-云平台配置信息)   成功时为云平台配置信息；失败时为nil

- [Status](#status-标准错误码定义)      错误状态

示例

```lua
local cfg, err = cloud.GetCloudPlatformConfig(0)
if err:IsFailed() then
  return
end

local request = {
  URL = cfg.HttpHost .. clouds.Command.REPORT_DRIVE_IN_ADDR,
  ContentType = clouds.Config.contentType,
  Body = getReportData(clouds.Command.REPORT_DRIVE_IN, cfg, carInfo, true, entID)
}
```

##### GetMonthlyRentals 获取月租信息

```lua
cloud.GetMonthlyRentals()
```

参数

- filter [MonthlyRentalFilter](#monthlyrentalfilter-月租用户过滤器) 月租用户过滤器

返回值

- [MonthlyRental](#monthlyrental-月租卡用户信息)[]  成功时为月租卡用户信息数组；失败时为nil

- [Status](#status-标准错误码定义)      错误状态

##### SetReportLog 设置上报日志

```lua
cloud.SetReportLog(materialID, park, info)
```

参数

- materialID  int64   物料ID
- park  int  是否为停车
- info string 出入库信息，一般为上报响应结果json字符串

返回值

- [Status](#status-标准错误码定义)      错误状态

示例

```lua
-- 车辆驶入信息上报
function clouds.ReportDriveIn(materialID, entID)
  -- 获取云平台配置信息
  local cfg, err = cloud.GetCloudPlatformConfig(0)
  if err:IsFailed() then
    return
  end

  local carInfo, res, desc = car.GetCarInfoByMaterialID(materialID)
  if not res or (carInfo == nil) or (carInfo.Id <= 0) then
    return
  end

  x1print("ParkTag:", carInfo.Properties.ParkTag, "entID:", entID)
  local request = {
    URL = cfg.HttpHost .. clouds.Command.REPORT_DRIVE_IN_ADDR,
    ContentType = clouds.Config.contentType,
    Body = getReportData(clouds.Command.REPORT_DRIVE_IN, cfg, carInfo, true, entID)
  }

  local response, result = httpPost(request)
  if not result then
    task.SetGP(clouds.Config.reportAllFlag, -1)
  else
    clouds.UpdateResidual(cfg)
  end

  task.SetGP(clouds.Config.carChanged, os.time())
  cloud.SetReportLog(carInfo.Id, true, response.Body)
end
```

##### GetReportLog 获取上报日志

```lua
cloud.GetReportLog (materialID, park)
```

参数

- materialID  int64   物料ID
- park  int  是否为停车

返回值

- string   出入库信息，SetReportLog()所保存的info

- [Status](#status-标准错误码定义)    错误状态

示例

```lua
-- 车辆驶离信息上报
function clouds.ReportDriveOut(materialID, entID)
  -- 获取云平台配置信息
  local cfg, err = cloud.GetCloudPlatformConfig(0)
  if err:IsFailed() then
    return
  end

  local carInfo, res, desc = car.GetCarInfoByMaterialID(materialID)
  if not res or (carInfo == nil) or (carInfo.Id <= 0) then
    return
  end

  x1print("ParkTag:", carInfo.Properties.ParkTag, "entID:", entID)
  local request = {
    URL = cfg.HttpHost .. clouds.Command.REPORT_DRIVE_OUT_ADDR,
    ContentType = clouds.Config.contentType,
    Body = getReportData(clouds.Command.REPORT_DRIVE_OUT, cfg, carInfo, false, entID)
  }

  local response, result = httpPost(request)
  if not result then
    task.SetGP(clouds.Config.reportAllFlag, -1)
  else
    clouds.UpdateResidual(cfg)
  end

  task.SetGP(clouds.Config.carChanged, os.time())
  cloud.SetReportLog(materialID, false, response.Body)
end
```

### config 配置

#### 系统模式

当前系统，只有自动模式和维护模式两种，定义如下

| 名称          | 值   | 含义          |
| ------------- | ---- | ------------- |
| config.Auto   | 1    | 自动模式      |
| config.Manual | 0    | 手动/维护模式 |

#### 配置全局变量

- [mode](#系统模式)  int  系统模式

#### config 配置指令

##### IsAuto 系统是否处于自动模式

```lua
config.IsAuto()
```

返回值

- bool  是否处于自动模式
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local res, err = config.IsAuto()
if err:IsFailed() or (not res) then
  return desc_sendback(StatusCode.NORMAL, "In Maintenance", 3)
end
```

##### UpdateAuto 设置自动模式或维护模式

```lua
config.UpdateAuto(isAuto)
```

参数

- isAuto  bool  是否处于自动模式

返回值

- [Status](#status-标准错误码定义) 错误状态

##### Mode 获取系统模式

```lua
config.Mode()
```

返回值

- int    返回[系统模式](#系统模式)

- [Status](#status-标准错误码定义) 错误状态

##### SetMode 设置系统模式

```lua
config.SetMode(mode)
```

参数

- mode  int  设置[系统模式](#系统模式)

返回值

- [Status](#status-标准错误码定义) 错误状态

### ledscreen 条屏指令

#### Show 显示文本信息

```lua
ledscreen.Show(ledscreenID, areaIndex, content)
```

当前led屏仅支持**威视的LED屏**，不在对以前的励研LED屏进行支持。

参数

- ledscreenID  int  设备ID
- areaIndex  int  显示区域ID
- content  string  要显示的内容

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
if #stnList < 1 then
  opc.Write(1, common.EntranceSignal.PW_FULL_GARAGE, 1)
  ledscreen.Show(1, 2, string.format("空闲车位:" .. #stnList))
  ledscreen.Show(2, 2, string.format("空闲车位:" .. #stnList))
  print("满库中没有可用车位")
else
  opc.Write(1, common.EntranceSignal.PW_FULL_GARAGE, 0)
  print("stnList count: " .. #stnList)
  ledscreen.Show(1, 2, string.format("空闲车位:" .. #stnList))
  ledscreen.Show(2, 2, string.format("空闲车位:" .. #stnList))
end
```

#### Reset 重置

```lua
ledscreen.Reset(ledscreenID)
```

执行该指令后，设备会重启

参数

- ledscreenID  int  设备ID

返回值

- [Status](#status-标准错误码定义) 错误状态

#### TimeCalibration 时间校准

```lua
ledscreen.TimeCalibration(ledscreenID)
```

将设备时间校准到和MPS系统一致

参数

- ledscreenID  int  设备ID

返回值

- [Status](#status-标准错误码定义) 错误状态

### log 日志

#### 日志格式化

输出日志内容，格式符与golang语言一致，常见的格式化如下：

- %d int变量
- %x, %o, %b 分别为16进制，8进制，2进制形式的int
- %f, %g, %e 浮点数： 3.141593 3.141592653589793 3.141593e+00
- %t 布尔变量：true 或 false
- %c rune (Unicode码点)，Go语言里特有的Unicode字符类型
- %s string
- %q 带双引号的字符串 "abc" 或 带单引号的 rune 'c'
- %v 会将任意变量以易读的形式打印出来
- %T 打印变量的类型
- %% 字符型百分比标志（%符号本身，没有其他操作）

#### 日志级别

| 值   | 名列       | 含义         |
| ---- | ---------- | ------------ |
| 1    | FatalLevel | 严重错误级别 |
| 2    | ErrorLevel | 错误级别     |
| 3    | WarnLevel  | 警告级别     |
| 4    | InfoLevel  | 信息级别     |
| 5    | DebugLevel | 调试级别     |
| 6    | TraceLevel | 跟踪级别     |

#### 日志指令

##### SetLevel 设置日志级别

```lua
log.SetLevel(level)
```

参数

- [level](#日志级别)  int  日志级别  

##### 输出日志

```lua
log.Fatal(content)      -- 输出严重错误日志
log.Error(content)      -- 输出错误日志
log.Info(content)       -- 输出信息日志
log.Warning(content)    -- 输出警告日志
log.Debug(content)      -- 输出调试日志
log.Trace(content)      -- 输出跟踪日志
```

输出相应级别的日志

参数

- content  string  要输出的日志内容

示例

```lua
log.Info("Stop agvID:"..agvID)
log.Warningf("GoIn, error:%s", desc)
```

##### 输出日志-带格式化

```lua
log.Fatalf(format, ...)     -- 输出格式化严重错误日志
log.Errorf(format, ...)     -- 输出格式化错误日志
log.Infof(format, ...)      -- 输出格式化信息日志
log.Warningf(format, ...)   -- 输出格式化警告日志
log.Debugf(format, ...)     -- 输出格式化调试日志
log.Tracef(format, ...)     -- 输出格式化跟踪日志
```

输出格式化的严重错误级别日志

参数

- format  string  格式化参数，参考log - 格式化
- ...  string|number|bool  可选变量，变量个数与类型需与format的格式保持一致。

示例

```lua
log.Infof("RequestTake entranceID: %d, parkingTag: %s, orderTime: %s", entranceID, parkingTag, orderTime)
log.Warningf("TryScheduing(materialID=%d), AllocateEntrance failed", materialID)
```

### lpr 车牌识别指令

#### GetLicensePlate 获取当前已识别的车牌号

```lua
lpr.GetLicensePlate(id)
```

参数

- id  int   车牌识别摄像头编号，通常使用出入口编号

返回值

- string  车牌号

- [Status](#status-标准错误码定义)    错误状态

示例

```lua
-- 获取车牌号接口
function GetPlateNumber(entranceID, code)
  --车牌识别
  local caropt, err = lpr.GetLicensePlate(entranceID) 
  if err:IsFailed() or caropt == "" then
    return desc_sendback(2, "", 0)
  end

  return desc_sendback(1, caropt, 0)
end
```

#### ClearLicensePlate 清除当前已识别的车牌号

```lua
lpr.ClearLicensePlate(entraceID)
```

参数

- id  int   车牌识别摄像头编号，通常使用出入口编号

返回值

- [Status](#status-标准错误码定义)    错误状态

示例

```lua
--清除车牌识别结果
lpr.ClearLicensePlate(entranceID)
```

### material 物料

#### 物料数据类型

##### Material 物料信息

指令结构Material，结合[materialWrapper.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/material/materialWrapper.go) DecodeMaterial()与MaterialPb.Material

```go
type Material struct {
  Id         int64    // 物料id
  TypeName  string    // 物料类型名称    
  TypeID     string   // 物料类型ID    
  CellID     uint32   // 所在储位
  Properties string   // 物料属性    
}
```

##### MaterialFilter 物料过滤器

指令结构 MaterialFilter，参考[materialFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/material/materialFilter.go) MaterialFilter

```go
type MaterialFilter struct {
  And         []*MaterialFilter   // 过滤器 And
  Or          []*MaterialFilter   // 过滤器 Or
  TypeID      *uint32             // 类型ID
  IDIn        []int64             // 物料ID包括
  CellIDNotIn []uint32            // 储位不包括
  Property    []*PropertyFilter   // 属性过滤
}
```

##### PropertyFilter 物料过滤器属性

指令结构 PropertyFilter，参考[materialFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/material/materialFilter.go) PropertyFilter

```go
type PropertyFilter struct {
  Key     string        // 属性名称
  Value   interface{}   // 属性值
  IsFuzzy bool          // 是否使用模糊查询(like)
}
```

#### 物料指令

##### IsExist 物料是否存在

```lua
material.IsExist(materialID)
```

参数

- materialID  int64  物料ID

返回值

- bool  物料是否存在

- [Status](#status-标准错误码定义) 错误状态

##### GetMaterial 获取指定物料信息

```go
material.GetMaterial(materialID)
```

用于获取物料信息，其返回的数据为lua的table类型，以属性名称作为key，属性值作为value。

物料的主要内容放在属性数组Properties中，这是因为物料的信息是根据项目录入进去的，需要根据项目定制。

参数

- materialID  int64  物料ID

返回值

- [Material](#material-物料信息)  物料信息
- [Status](#status-标准错误码定义)        错误状态

示例

```lua
-- 根据物料编号获取车辆信息
function car.GetCarInfoByMaterialID(materialID)
  carInfo, err = material.GetMaterial(materialID)
  if err:IsFailed() then
    return nil, false, err:String()
  end
  if carInfo == nil then
    return nil, false, "carInfo is nil"
  end

  return carInfo, true, "ok"
end
```

##### GetMaterials 查询物料

```go
material.GetMaterials(filter)
```

参数

- filter  [MaterialFilter](#propertyfilter-物料过滤器属性)  物料过滤器

返回值

- [Material](#material-物料信息)[]  物料信息数组
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
-- 获取所有在库的车辆
function car.GetAllInGarage()
  materials, err = material.GetMaterials({CellIDNotIn = {0}})
  if err:IsFailed() then
    return nil, false, err:String()
  end

  return materials, true, "ok"
end
```

##### DeleteMaterial 删除指定物料

```lua
material.DeleteMaterial(materialID)
```

参数

- materialID  int64  物料ID

返回值

- [Status](#status-标准错误码定义) 错误状态

##### DeleteMaterials 批量删除物料

```lua
material.DeleteMaterials(materialIDs)
```

参数

- materialIDs  int64[]  物料ID数组

返回值

- [Status](#status-标准错误码定义) 错误状态

##### AddMaterial 添加一个物料

```lua
material.AddMaterial(typeID, properties)
```

参数

- typeID  int64  物料类型编号
- properties  table  物料属性，设置Material.Properties

返回值

- int64  物料ID
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local materialID, err = material.AddMaterial(car.Props.CarTypeID, carData)
if err:IsFailed() then
  return false, err:String()
end
```

##### AddMaterials 批量添加物料

```lua
material.AddMaterials(properties)
```

参数

- properties  table[]  物料属性数组，设置Material.Properties

返回值

- [Status](#status-标准错误码定义) 错误状态

##### UpdateMaterial 更新物料信息

```lua
material.UpdateMaterial(materialID, properties)
```

参数

- materialID  int64  物料ID
- properties  table  物料属性，设置Material.Properties

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local err = material.UpdateMaterial(carInfo.Id, carData)
if err:IsFailed() then
  return false, err:String()
end
```

##### SetProperty 设置物料属性

```lua
material.SetProperty(materialID, arg, argv)
```

参数

- materialID  int64  物料ID
- arg  string  属性名称
- argv  string|number|bool  属性值

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
-- 车辆离开，设置车辆离开时间
function car.LeaveOut(materialID)
  local date = os.date("%Y-%m-%d %H:%M:%S")
  local err = material.SetProperty(materialID, car.Props.OutTime, date)
  if err:IsFailed() then
    return false, err:String()
  end

  return true, "ok"
end
```

##### GetProperty 获取物料属性

```lua
material.GetProperty(materialID, arg)
```

参数

- materialID  int64  物料ID
- arg  string  属性名称

返回值

- string|number|bool  属性值

- [Status](#status-标准错误码定义) 错误状态

### notification 通知指令

#### Publish 发送通知

```lua
notification.Publish(format, ...)
```

格式化字符串格式请参考[格式化](#日志格式化)

该指令会向消息中心发送一个通知，并在页面的右上角的通知中心中显示，用于提醒管理员。

目前系统内置的通知包括两种：

- 脚本执行出错时的通知
- 任务被取消时的通知

参数

- format  string  要推送的内容，支持格式化字符串

返回值

- [Status](#status-标准错误码定义)      错误状态

### opc PLC信号指令

#### Read 读取PLC数据

```lua
opc.Read(group, item)
```

参数

- group  int  OPC组编号
- item   int  OPC组中的项编号

返回值

- int    读取到PLC的值
- [Status](#status-标准错误码定义)  错误状态

示例

```lua
local entStatus, err = opc.Read(entranceID, common.EntranceSignal.PR_ENTRANCE_STATUS)
if err:IsFailed() then
  return desc_sendback(StatusCode.NETWORK_ERROR, "Signal Error", 1)
end
```

#### Write 写入PLC数据

```lua
opc.Write(group, item, value)
```

写入PLC数据，当为bool类型时：

- 用1表示true
- 用0表示false

参数

- group  int  OPC组编号
- item   int  OPC组中的项编号
- value  int  要写入的值

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
-- entranceID是出入口号 ,orderID是口号,command是命令, liftFloor是电梯楼层
function entrance.SendOrder(entranceID, orderID, command, liftFloor)
  -- 口号  车位号  车位类型   命令  升降机目标位置  动作启动   动作启动确认
  local err = opc.Write(entranceID, common.EntranceSignal.PW_TASK_NUMBER, orderID)
  if err:IsFailed() then
    return false
  end
  err = opc.Write(entranceID, common.EntranceSignal.PW_TASK_CARNUMBER, 4)
  if err:IsFailed() then
    return false
  end
  -- 车位类型只有短小车(无信息:0 短小车:1 短大车:2 长小车:3 长大车:4)
  err = opc.Write(entranceID, common.EntranceSignal.PW_TASK_CARTYPE, 1)
  if err:IsFailed() then
    return false
  end
  err = opc.Write(entranceID, common.EntranceSignal.PW_TASK_COMMAND, command)
  if err:IsFailed() then
    return false
  end

  local err = opc.Write(entranceID, common.EntranceSignal.PW_AIM_FLOOR, liftFloor)
  if err:IsFailed() then
    return false
  end

  err = opc.Write(entranceID, common.EntranceSignal.PW_START_TASK, 1)
  if err:IsFailed() then
    return false
  end

  err = opc.Write(entranceID, common.EntranceSignal.PW_START_TASK_SURE, 1)
  if err:IsFailed() then
    return false
  end

  return true
end
```

#### 逻辑运算符

```lua
opc.IsTrue(group, item)             -- 值是否非零/true
opc.IsFalse(group, item)            -- 值是否为零/false
opc.Equal(group, item, value)       -- 值是否等于某个值
opc.Less(group, item, value)        -- 值是否小于某个值
opc.LessEqual(group, item, value)   -- 值是否小于等于某个值
opc.Greater(group, item, value)     -- 值是否大于某个值
opc.GreaterEqual(group, item, value)-- 值是否大于等于某个值
```

参数

- group  int    OPC组编号
- item   int  OPC组中的项编号
- value  int  期望值

返回值

- bool  比较结果
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local res, err = opc.IsTrue(1, common.EntranceSignal.PR_GUIDE_OVER_LENGTH)
local v, err = opc.Equal(entranceID, common.EntranceSignal.PW_TASK_NUMBER, orderID)
```

### rack 货架

#### 货架数据类型

##### Rack 货架信息

指令结构Rack，结合[rackWrapper.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/rack/rackWrapper.go) DecodeRack() 与rackPb.Rack

```go
type Rack struct {
  Id   uint32     // 货架编码
  IsValid bool    // 是否禁用
  IsOccupied bool // 是否被占用
  RacklotID uint32// 所在货位
  Layers uint32   // 货架层数
  Length uint32   // 货架长
  Width  uint32   // 货架宽
  Height uint32   // 货架高
  Type string     // 货架类型名称
  Cells []*Cell   // 货架储位
}
```

##### Cell 储位信息

指令结构Cell，结合[rackWrapper.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/rack/rackWrapper.go) DecodeCell() 与rackPb.Cell

```go
type Cell struct {
  Id         uint32   // 储位ID
  Tag        string   // 储位编码
  Name       string   // 储位名称
  Row        uint32   // 储位在货架的哪一层
  Column     uint32   // 储位在货架上的编号
  Length     uint32   // 储位长
  Width      uint32   // 储位宽
  Height     uint32   // 储位高
  Status     int32    // 储位状态
  RackID     uint32   // 储位所在货架
  IsValid    bool     // 是否禁用
  IsOccupied bool     // 是否被占用
  MaterialID int64    // 存放的物料对于的编号
}
```

##### RackFilter 货架过滤器

指令结构 RackFilter，参考[rackFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/rack/rackFilter.go) RackFilter

```go
type RackFilter struct {
  And         []*RackFilter   // 过滤器 And  
  Or          []*RackFilter   // 过滤器 Or
  IDIn        []uint32        // ID 包括
  RacklotIDIn []uint32        // 货位ID 包括
  RacklotID   *uint32         // 货位ID
  IsValid     *bool           // 是否有效
  IsOccupied  *bool           // 是否已占用
  Type        *RackTypeFilter // 货架类型过滤器
  Cell        *CellFilter     // 储位过滤器
}
```

##### RackTypeFilter 货架类型过滤器

指令结构 RackTypeFilter，参考[rackFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/rack/rackFilter.go) RackTypeFilter

```go
type RackTypeFilter struct {
  And    []*RackTypeFilter  // 过滤器 And
  Or     []*RackTypeFilter  // 过滤器 Or
  IDIn   []uint32           // 类型ID 包括
  NameIn []string           // 名称包括
  ID     *uint32            // ID 
  Name   *string            // 名称
}
```

##### CellFilter 储位过滤器

指令结构 CellFilter，参考[rackFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/rack/rackFilter.go) CellFilter

```go
type CellFilter struct {
  And           []*CellFilter // 过滤器 And  
  Or            []*CellFilter // 过滤器 Or
  ID            *uint32       // ID    
  IDIn          []uint32      // ID 包括   
  Name          *string       // 名称 
  NameIn        []string      // 名称包括   
  MaterialIDIn  []int64       // 物料ID 包括
  MaterialID    *int64        // 物料ID
  IsHasMaterial *bool         // 储位是否有物料
  RackID        *uint32       // 货架ID   
  IsValid       *bool         // 是否有效   
  IsOccupied    *bool         // 是否已占用    
  Property      []*PropertyFilter  // 属性过滤器
}
```

##### PropertyFilter 储位过滤器属性

指令结构 PropertyFilter，参考[rackFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/rack/rackFilter.go) PropertyFilter

```go
type PropertyFilter struct {
  Key   string      // 名称
  Value interface{} // 值
}
```

#### 货架指令

##### IsExist 货架是否存在

```lua
rack.IsExist(rackID)
```

参数

- rackID  int  货架ID

返回值

- [Status](#status-标准错误码定义) 错误状态

##### IsAvailable 货架是否可用

```lua
rack.IsAvailable(rackID)
```

参数

- rackID  int  货架ID

返回值

- bool  货架是否可用
- [Status](#status-标准错误码定义) 错误状态

##### GetRack 获取指定货架信息

```lua
rack.GetRack(rackID)
```

参数

- rackID  int  货架ID

返回值

- [Rack](#rackfilter-货架过滤器)  货架信息
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local rackInfo, err = rack.GetRack(racklots[1].RackID)
```

##### GetRacks 查询满足条件的货架

```lua
rack.GetRacks(filter)
```

参数

- filter  RackFilter  货架过滤器

返回值

- [Rack](#rack-货架信息)[] 货架信息数组
- [Status](#status-标准错误码定义) 错误状态

##### GetRacklot 获取货架所在货位

```lua
rack.GetRacklot(rackID)
```

参数

- rackID  int  货架ID

返回值

- int    货位ID
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
-- 根据储位ID，获取取货位ID
function parkinglot.GetParkinglotIDByCellID(cellID)
  racks, err = rack.GetRacks({Cell = {ID = cellID}})
  if err:IsFailed() then
    return 0, false, err:String()
  end

  if #racks <= 0 then
    return 0, false, "no such rack"
  end

  return racks[1].RacklotID, true, "ok"
end
```

##### GetLayer 获取货架层数

```lua
rack.GetLayer(rackID)
```

参数

- rackID  int  货架ID

返回值

- int    货架层数
- [Status](#status-标准错误码定义) 错误状态

##### GetPickingSurface 获取货架的AB面配置

```lua
rack.GetPickingSurface(rackID)
```

参数

- rackID  int  货架ID

返回值

- int    0: 表示只有一个拣货面；1: 表示有两个拣货面
- [Status](#status-标准错误码定义) 错误状态

##### GetStatus 获取货架状态

```lua
rack.GetStatus(rackID)
```

参数

- rackID  int  货架ID

返回值

- int    定义尚未明确
- [Status](#status-标准错误码定义) 错误状态

##### Enable/Disable 启用/禁用货架

```lua
rack.Enable(rackID)   -- 启用货架
rack.Disable(rackID)  -- 禁用货架
```

参数

- rackID  int  货架ID

返回值

- [Status](#status-标准错误码定义) 错误状态

##### BindMaterial/UnBindMaterial 货架绑定/解绑物料

```lua
rack.BindMaterial(cellID, materialID) -- 货架绑定物料
rack.UnBindMaterial(cellID)           -- 货架解绑物料
```

货架的最小存储单元是储位cell，因此物料绑定的是储位而不是货架

参数

- cellID  int  储位ID
- materialID  int64  物料ID

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local err = rack.BindMaterial(cellID, carInfo.Id)
if err:IsFailed() then
  return false, "bind failed"
end

-- 根据物料ID移除车辆
function car.RemoveCarByMaterialID(materialID)
  carInfo, res, desc = car.GetCarInfoByMaterialID(materialID)
  if not res then
    return false, desc
  end
  if carInfo.CellID <= 0 then
    return false, "cellID is 0"
  end

  err = rack.UnBindMaterial(carInfo.CellID)
  if err:IsFailed() then
    return false, err:String()
  end

  return true, "ok"
end
```

##### HasMaterial 储位上是否有物料

```lua
rack.HasMaterial(cellID)
```

参数

- cellID  int  储位ID

返回值

- bool  储位上是否有物料
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local isHasMaterial, err = rack.HasMaterial(racklots[r].RackID)
if err:IsFailed() then
  return false
end
```

##### IsOccupiedCell 储位是否被占用

```lua
rack.IsOccupiedCell(cellID)
```

参数

- cellID  int  储位ID

返回值

- bool  储位是否被占用

- [Status](#status-标准错误码定义) 错误状态

##### OccpuyCell/ReleaseCell 占用/释放储位

```lua
rack.OccpuyCell(cellID)   -- 占用储位
rack.ReleaseCell(cellID)  -- 释放占用储位
```

参数

- cellID  int  储位ID

返回值

- [Status](#status-标准错误码定义) 错误状态

##### GetCell 获取指定储位

```lua
rack.GetCell(cellID)
```

参数

- cellID  int  储位ID

返回值

- [Cell](#cell-储位信息)  储位的信息

- [Status](#status-标准错误码定义) 错误状态

##### GetCells 查询满足条件的储位

```lua
rack.GetCells(filter)
```

参数

- filter  [CellFilter](#rackfilter-货架过滤器)  储位过滤器

返回值

- [Cell](#cell-储位信息)[]  储位信息数组
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
-- 根据储位ID查找物料
function car.GetCarInfoByCellID(cellID)
  cells, err = rack.GetCells({ID = cellID})

  if err:IsFailed() or #cells < 1 then
    return nil, false, "GetCarInfoByCellID no such cell"
  end

  carInfo, res, desc = car.GetCarInfoByMaterialID(cells[1].MaterialID)
  if not res then
    return nil, false, desc
  end

  return carInfo, true, "ok"
end
```

##### ~~SetCellStatus 设置储位状态~~

```lua
rack.SetCellStatus(cellID, status)
```

参数

- cellID  int  储位ID
- status  int  储位状态，尚未明确定义

返回值

- [Status](#status-标准错误码定义) 错误状态

##### GetCellStatus 获取储位状态

参数

- cellID  int  储位ID

返回值

- int  储位状态，尚未明确定义

- [Status](#status-标准错误码定义) 错误状态

### racklot 货位

#### 货位数据类型

##### Racklot 货位信息

指令结构Racklot，结合[racklotWrapper.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/racklot/racklotWrapper.go) DecodeRacklot() 与racklotPb.Racklot

```go
type Racklot struct {
  Id            uint32// ID
  IsValid       bool  // 站台是否有效，被禁用则无效，启用则有效，默认为有效
  IsInbound     bool  // 是否被占用
  IsOutbound    bool  // 是否允许出库
  IsOccupied    bool  // 是否允许入库
  StationID     uint32// 站台ID
  RackID        uint32// 关联的货架ID，在货位上没有放置货架时为0
  MultipleGroup *MultipleGroup// 站台重列分组信息
}
```

##### RacklotFilter 货位过滤器

指令结构RacklotFilter，参考[racklotFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/racklot/racklotFilter.go) RacklotFilter

```go
type RacklotFilter struct {
  And                []*RacklotFilter // 过滤器 And
  Or                 []*RacklotFilter // 过滤器 Or
  IDIn               []uint32 // ID 包括      
  IDNotIn            []uint32 // ID 不包括       
  RackIDIn           []uint32 // 货架ID 包括       
  RackIDNotIn        []uint32 // 货架ID 不包括      
  StationID          *uint32  // 站台ID      
  StationIDIn        []uint32 // 站台ID 包括     
  RackID             *uint32  // 货架ID   
  IsInbound          *bool    // 是否允许入库        
  IsOutbound         *bool    // 是否允许出库        
  IsValid            *bool    // 是否有效         
  IsOccupied         *bool    // 是否已占用          
  Type               *RacklotTypeFilter  // 货位类型过滤器
  Property           []*PropertyFilter  // 属性过滤器
  MultipleGroup      *uint32  // 重列组
  MultipleParentItem *uint32  // 父重列号
  MultipleItem       *uint32  // 重列号
}
```

##### RacklotTypeFilter 货位类型过滤器

指令结构RacklotTypeFilter，参考[racklotFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/racklot/racklotFilter.go) RacklotTypeFilter

```go
type RacklotTypeFilter struct {
  And    []*RacklotTypeFilter  // 过滤器 And
  Or     []*RacklotTypeFilter  // 过滤器 Or
  ID     *uint32  // ID
  Name   *string  // 名称
  IDIn   []uint32 // ID 包括
  NameIn []string // 名称包括   
}
```

##### PropertyFilter 货位类型属性

指令结构PropertyFilter，参考[racklotFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/racklot/racklotFilter.go) PropertyFilter

```go
type PropertyFilter struct {
  Key   string      // 名称
  Value interface{} // 值
}
```

#### 货位指令

##### IsExist 位是否存在

```lua
racklot.IsExist(racklotID)
```

参数

- racklotID  int  货位ID

返回值

- [Status](#status-标准错误码定义) 错误状态

##### IsAvailable 货位是否可用

```lua
racklot.IsAvailable(racklotID)
```

参数

- racklotID  int  货位ID

返回值

- bool  货位是否可用

- [Status](#status-标准错误码定义) 错误状态

##### GetRacklot 获取指定货位

```lua
racklot.GetRacklot(racklotID)
```

参数

- racklotID  int  货位ID

返回值

- [Racklot](#racklot-货位信息)  货位信息

- [Status](#status-标准错误码定义)      错误状态

##### GetRacklots 查询货位信息

```lua
racklot.GetRacklots(filter)
```

参数

- filter  [RacklotFilter](#racklotfilter-货位过滤器)  货位过滤器

返回值

- [Racklot](#racklot-货位信息)[]  货位信息数组
- [Status](#status-标准错误码定义)         错误状态

示例

```lua
-- 获取出入口的货位
function entrance.GetParkinglot(entranceID, isEntry)
  entranceLots, err = racklot.GetRacklots({
      Type = {Name = "ParkingSpace"},
      Property = {{
          Key = entrance.Props.EntranceID,
          Value = entranceID
        }, {
          Key = entrance.Props.IsEntry,
          Value = isEntry
        }}
    })

  if err:IsFailed() then
    return nil, false, err:String()
  end

  if #entranceLots <= 0 then
    return nil, false, "no such lot"
  end

  -- 返回第一个对象，通常情况下也只有1个
  return entranceLots[1], true, "ok"
end
```

##### Enable/Disable 启用/禁用货位

```lua
racklot.Enable(racklotID)   -- 启用货位
racklot.Disable(racklotID)  -- 禁用货位
```

参数

- racklotID  int  货位ID

返回值

- [Status](#status-标准错误码定义)      错误状态

##### Occupy/Release 占用/释放货位

```lua
racklot.Occupy(racklotID)   -- 占用货位
racklot.Release(racklotID)  -- 释放占用货位
```

参数

- racklotID  int  货位ID

返回值

- [Status](#status-标准错误码定义)      错误状态

示例

```lua
local err = racklot.Occupy(parkinglotID)
local err = racklot.Release(deliverlot.Id)
```

##### IsOccupied 货位是否已占用

```lua
racklot.IsOccupied(racklotID)
```

参数

- racklotID  int  货位ID

返回值

- bool    货位是否被占用
- [Status](#status-标准错误码定义)      错误状态

示例

```lua
 local isOccupide, err1 = racklot.IsOccupied(entLot.Id)
```

##### GetStation 获取货位绑定的站台

```lua
racklot.GetStation(racklotID)
```

参数

- racklotID  int  货位ID

返回值

- int      货位绑定的站台ID

- [Status](#status-标准错误码定义)      错误状态

##### BindRack/UnbindRack 绑定/解绑货架

```lua
racklot.BindRack(racklotID, rackID)   -- 绑定货架
racklot.UnbindRack(racklotID, rackID) -- 解绑货架
```

参数

- racklotID  int  货位ID
- rackID  int  货架ID

返回值

- [Status](#status-标准错误码定义)      错误状态

示例

```lua
-- 绑定货架
local err = racklot.BindRack(entryLot.Id, exitLot.RackID)
if err:IsFailed() then
  task.SetDescription("BindRack failed")
  return false
end

-- 解绑货架
err = racklot.UnbindRack(exitLot.Id, exitLot.RackID)
if err:IsFailed() then
  task.SetDescription("UnbindRack failed")
  return false
end
```

##### SetProperty 设置货架属性

```lua
racklot.SetProperty(racklotID, arg, argv)
```

参数

- racklotID  int  货位ID
- arg  string  属性名称
- argv  string|number|bool  属性值

返回值

- [Status](#status-标准错误码定义)      错误状态

##### GetProperty 获取货架属性

```lua
racklot.GetProperty(racklotID, arg)
```

返回的类型与调用racklot.SetProperty(racklotID, arg, argv)相同

参数

- racklotID  int  货位ID

- arg  string  属性名称

返回值

- string|number|bool  属性值
- [Status](#status-标准错误码定义)      错误状态

示例

```lua
function parkinglot.GetEntranceID(racklotID)
  val, err = racklot.GetProperty(racklotID, "EntranceID")
  if err:IsFailed() then
    return 0, false, err:String()
  end

  return tonumber(val), true, "ok"
end
```

##### GetMultipleGroupRacklots 获取重列的货位

```lua
racklot.GetMultipleGroupRacklots(racklotID)
```

根据重列条件获取重列货位，这些货位属于指定货位的外层，搬运重列组中货位上的货架时，需先将外层货位上的货架移开，然后才能搬运。

参数

- racklotID  int  货位ID

返回值

- [Racklot](#racklot-货位信息)[]  外层货位数组，按照从最外层向内层的顺序返回
- [Status](#status-标准错误码定义)      错误状态

##### OccupyMultipleGroup/ReleaseMultipleGroup 占用/释放重列组

```lua
racklot.OccupyMultipleGroup(racklotID)  -- 占用重列组
racklot.ReleaseMultipleGroup(racklotID) -- 释放重列组
```

根据货位id占用该货位所在的重列组，只有该货位所在的重列组中所有货位均未被占用，才可以占用，否则就占用失败，并返回错误码status.ErrOccupied表示该重列组已被占用

参数

- racklotID  int  货位ID

返回值

- [Status](#status-标准错误码定义)      错误状态

##### IsOccupiedMultipleGroup 重列组是否被占用

```lua
racklot.OccupyMultipleGroup(racklotID)
```

参数

- racklotID  int  货位ID

返回值

- bool  重列组是否被占用

- [Status](#status-标准错误码定义)      错误状态

### route 路径

#### 路径数据类型

##### StationWeight 站台权重信息

指令结构StationWeight，结合[routeWrapper.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/route/routeWrapper.go) DecodeWeight() 与routePb.StationWeight

```go
type StationWeight struct {
  StationID uint32 
  Weight    uint32 
}
```

#### 路径指令

##### ShowRoute/HideRoute 显示/隐藏路径

```lua
route.ShowRoute(startStn, endStn) -- 显示路径
route.HideRoute(startStn, endStn) -- 隐藏路径
```

参数

- startStn  int  起始站台
- endStn  int  结束站台

返回值

- [Status](#status-标准错误码定义)      错误状态

示例

```lua
 route.ShowRoute(fetchStn, deliverStn)
```

##### Search 搜索站台路径

```lua
route.Search(startStn, endStns)
```

搜索站台到指定站台队列的权重排序

参数

- startStn  int  起始站台
- endStns  int[]  结束站台数组

返回值

- [StationWeight](#stationweight-站台权重信息)[]  站台权重数组
- [Status](#status-标准错误码定义)      错误状态

示例

```lua
--根据路径算法计算 存车位
local sortedStations, err = route.Search(entStn, stnList)
if err:IsFailed() or #sortedStations < 1 then
  print("没有可用的车位2: " .. err:String())
  return 0, 0, 0, false, err:String()
end
```

##### GetStnList 获取站台可达的所有站台权重

```lua
route.GetStnList(startStn)
```

站台到所有通行站台的权重排序

参数

- startStn  int  起始站台

返回值

- [StationWeight](#stationweight-站台权重信息)[]  站台权重数组，所有可通行站台数组，按权重从小到大排列

- [Status](#status-标准错误码定义)      错误状态

### scheduler 调度系统

参考资料：

- 《[怡丰机器人AGV调度系统与上位系统通信协议.docx](https://codeup.aliyun.com/yfrobots/mps/MPS-documents/blob/0a8433e2eeff483fae20f1d3a1a68672fc73b217/%E9%80%9A%E8%AE%AF%E5%8D%8F%E8%AE%AE/%E6%80%A1%E4%B8%B0%E6%9C%BA%E5%99%A8%E4%BA%BAAGV%E8%B0%83%E5%BA%A6%E7%B3%BB%E7%BB%9F%E4%B8%8E%E4%B8%8A%E4%BD%8D%E7%B3%BB%E7%BB%9F%E9%80%9A%E4%BF%A1%E5%8D%8F%E8%AE%AE.docx?spm=a2cl9.codeup_devops2020_goldlog_projectFiles.0.0.30bb440bgbfg7D&file=%E6%80%A1%E4%B8%B0%E6%9C%BA%E5%99%A8%E4%BA%BAAGV%E8%B0%83%E5%BA%A6%E7%B3%BB%E7%BB%9F%E4%B8%8E%E4%B8%8A%E4%BD%8D%E7%B3%BB%E7%BB%9F%E9%80%9A%E4%BF%A1%E5%8D%8F%E8%AE%AE.docx)》
- 《[上位系统和NDC TS交互流程.docx](https://codeup.aliyun.com/yfrobots/mps/MPS-documents/blob/8855d0b0086dbbdf2311d23c80fa27660e872d1e/%E9%80%9A%E8%AE%AF%E5%8D%8F%E8%AE%AE/%E4%B8%8A%E4%BD%8D%E7%B3%BB%E7%BB%9F%E5%92%8CNDC_TS%E4%BA%A4%E4%BA%92%E6%B5%81%E7%A8%8B.docx?spm=a2cl9.codeup_devops2020_goldlog_projectFiles.0.0.3ffc6002FK9gcw&file=%E4%B8%8A%E4%BD%8D%E7%B3%BB%E7%BB%9F%E5%92%8CNDC_TS%E4%BA%A4%E4%BA%92%E6%B5%81%E7%A8%8B.docx)》

- 《[System Manager Order Manager.pdf](https://codeup.aliyun.com/yfrobots/mps/MPS-documents/blob/8855d0b0086dbbdf2311d23c80fa27660e872d1e/%E9%80%9A%E8%AE%AF%E5%8D%8F%E8%AE%AE/System_Manager_Order_Manager.pdf)》

#### scheduler 调度系统指令

##### SendQAMessage 发送a格式的Q消息

```lua
scheduler.SendQAMessage(tsID, params)
```

参数

- tsID  int  脚本编号
- params  int[]  参数数组，至少包含一个数据

返回值

- [Status](#status-标准错误码定义)      错误状态

示例

```lua
-- 急停
local err = scheduler.SendQAMessage(4, {agvID, 1})

-- 急停恢复
local err = scheduler.SendQAMessage(4, {agvID, 0})

-- 全部急停
local err = scheduler.SendQAMessage(19, {1})

-- 打开对应楼层Z段
local err = scheduler.SendQAMessage(205, {1, agvFloor, tonumber(fetchLotFloor)})
```

##### SendQMessage 发送b格式的Q消息

```lua
scheduler.SendQMessage(tsID, priority, code, ikey, params)
```

参数

- tsID  int  脚本编号

- priority  int  任务优先级，范围[128,227]。<128 会设置为128；>227 会设置为227

- code  int  功能码，默认为1

- ikey  int  ikey值，一般存储MPS任务号

- params  int[]  参数数组，至少包含一个数据

返回值

- [Status](#status-标准错误码定义)      错误状态

示例

```lua
-- 发送AGV命令
local err = scheduler.SendQMessage(1, 128, 1, TaskID, {TaskID, fetchStn, deliverStn})
```

##### SendMMessage 发送M消息

```lua
scheduler.SendMMessage(index, funCode, parNo, params)
```

参数

- index    int  调度系统任务索引
- funCode  int  功能码，默认为1
- parNo    int  起始参数地址
- params  int[]  参数数组，至少包含一个数据

返回值

- [Status](#status-标准错误码定义)      错误状态

示例

```lua
-- 阶段2,参数检查阶段
function DoParameterCheck(index, phase)
  err = task.SetPhase(common.OrderPhase.ParameterCheck)
  if err:IsFailed() then
    return false
  end

  scheduler.SendMMessage(index, 1, 18, {phase})
  return true
end
```

##### SendNMessage 发送N消息

```lua
scheduler.SendNMessage(index)
```

参数

- index  int  调度系统任务索引

返回值

- [Status](#status-标准错误码定义)      错误状态

示例

```lua
local orderIndex, err = task.GetSchedulerOrderIndex()
if err:IsOK() and (orderIndex > 0) then
  scheduler.SendNMessage(orderIndex)
end
```

##### HostIsConnected 调度系统是否已连接

```lua
scheduler.HostIsConnected()
```

返回值

- [Status](#status-标准错误码定义)      错误状态

示例

```lua
-- 查看调度系统是否连接
local err = scheduler.HostIsConnected()
if err:IsFailed() then
  return false, "System manager is not connected", 16
end
```

### task 任务

#### 任务全局变量

| **变量名** | **类型** | **内容**                                                |
| ---------- | -------- | ------------------------------------------------------- |
| TaskID     | int      | 任务ID                                                  |
| Parameters | table    | 脚本的参数，类型为lua的字典表，可选参数，由task.New传入 |
| SMessageCh | LChannel | SMessage channel                                        |
| ExitCh     | LChannel | 退出使用的channel                                       |

#### 任务数据类型

##### Task 任务信息

指令结构Task，结合[taskWrapper.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/task/taskWrapper.go) DecodeTask()与taskPb.TaskInfo

```go
type Task struct {
  TaskID        uint32        // 任务ID
  Priority      uint32        // 任务优先级
  AgvID         uint32        // AGV编号
  AgvInitPoint  uint32        // AGV起始位置
  MaterialID    int64         // 任务关联的物料
  Phase         int32         // 任务阶段
  ScriptIndex   uint32        // 脚本编号
  OrderIndex    int32         // 调度系统任务ID
  OrderType     uint32        // 命令类型
  EntranceID    uint32        // 出入口编号
  RackID        uint32        // 货架ID
  OriginStation uint32        // 装货站台
  TargetStation uint32        // 卸货站台、目标站台
  ErrorCode     int32         // 错误码
  Description   string        // 任务描述
  BindAgvTime   string        // AGV任务绑定时间(AGV任务开始时间)
  DeliveryTime  string        // 卸货时间（AGV任务完成时间）
  Parameters    []*Parameter  // 任务局部参数
}
```

##### Parameter 任务局部参数

指令结构Parameter，结合[taskWrapper.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/task/taskWrapper.go) DecodeTask()与taskPb.Parameter

```go
type Parameter struct {
  Key   string // 参数名称
  Value string // 参数值
}
```

##### Agv 状态信息

指令结构Agv，结合[taskWrapper.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/task/taskWrapper.go) DecodeAGV()与agvPb.Agv

```go
type Agv struct {
  AgvID   uint32 // Agv ID
  Type    uint32 // Agv 类型
  X       uint32 // x 坐标
  Y       uint32 // y 坐标
  Angle   uint32 // 角度
  Point   uint32 // 点
  Segment uint32 // 段
  Battery uint32 // 站台
  Plc     uint32 // PLC 标识
  Status  uint32 // 状态
}
```

##### TaskFilter 任务过滤器

指令结构 TaskFilter，参考[taskFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/task/taskFilter.go) TaskFilter

```go
type TaskFilter struct {
  And            []*TaskFilter  // 过滤器 And    
  Or             []*TaskFilter  // 过滤器 Or
  ID             *uint32  // 任务ID        
  OrderType      *uint32  // 任务类型        
  OrderTypeIn    []int32  // 任务类型包括        
  OrderTypeNotIn []int32  // 任务类型不包括        
  IDIn           []int32  // 任务ID包括        
  Priority       *uint32  // 任务优先级        
  PriorityLT     *uint32  // 任务优先级小于       
  PriorityGT     *uint32  // 任务优先级大于      
  BeginLT        *string  // 任务开始时间小于     
  BeginGT        *string  // 任务开始时间大于   
  EndLT          *string  // 任务结束时间小于   
  EndGT          *string  // 任务结束时间大于      
  BindAgvTimeLT  *string  // AGV绑定时间小于   
  BindAgvTimeGT  *string  // AGV绑定时间大于      
  DeliveryTimeLT *string  // 投递时间小于   
  DeliveryTimeGT *string  // 投递时间大于      
  OrderIndex     *int32   // 任务索引    
  ScriptIndex    *uint32  // 脚本索引    
  Phase          *uint32  // 阶段   
  PhaseIn        []int32  // 阶段包括     
  PhaseNotIn     []int32  // 阶段不包括      
  PhaseGT        *uint32  // 阶段大于  
  PhaseLT        *uint32  // 阶段小于     
  PhaseGE        *uint32  // 阶段大于等于       
  PhaseLE        *uint32  // 阶段小于等于       
  EntranceID     *uint32  // 出入口编号       
  RackID         *uint32  // 货架ID      
  OriginStation  *uint32  // 源站点     
  TargetStation  *uint32  // 目标站点        
  AgvID          *uint32  // Agv ID         
  AgvInitPoint   *uint32  // AGV初始化所在点       
  MaterialID     *int64   // 物料ID      
  Parameter      *ParameterFilter  // 参数
  BindAgvTimeLT  *string  // AGV任务绑定时间(任务开始时间)
  BindAgvTimeGT  *string  // AGV任务绑定时间(任务开始时间)
  DeliveryTimeLT *string  // 卸货时间（AGV任务完成时间）
  DeliveryTimeGT *string  // 卸货时间（AGV任务完成时间）
}
```

##### ParameterFilter 过滤器参数

指令结构 ParameterFilter，参考[taskFilter.go](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-srv/instructions/task/taskFilter.go) ParameterFilter

```go
type ParameterFilter struct {
  Key   string      // 名称
  Value interface{} // 值
}
```

#### 任务指令

##### NewTask 创建新任务

```lua
task.NewTask(script, parameters, priority, id)
```

参数

- script  int|string  要执行的脚本编号或者脚本名称，名字区分大小写
- parameters  table  脚本所需的参数，类型为lua的字典表，可选参数
- priority  int  新任务的优先级，可选参数，默认为0
- id  int  新任务的ID，可选参数，未指定时自动生成taskID

返回值

- int    新任务ID
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
--  调用1号脚本，参数为{fetch=100, deliv=101, op=1}
id, err = task.NewTask(1, {fetch = 100, deliv = 101, op = 1})
if err:IsFailed() then
  --  生成任务失败，进行错误处理
end

params = {fetch = 100, deliv = 101, op = 1}

--  指定新任务ID为101,优先级为1，调用脚本"test.lua"
_, err = task.NewTask(test.lua, params, 1, 101)
if err:IsFailed() then
  --  生成任务失败，进行错误处理
end
```

##### GetTask 获取任务信息

```lua
task.GetTask(taskID)
```

参数

- taskID  uint32  任务ID

返回值

- [Task](#task-任务信息)  任务信息
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
t, err = task.GetTask(1)
log.Infof("Phase=%d", t.Phase)
```

##### GetTasks 根据条件查询任务信息

```lua
task.GetTasks(filter)
```

根据查询条件，查询满足条件的所有任务

参数

- filter  [TaskFilter](#taskfilter-任务过滤器)  任务过滤器

返回值

- [Task](#task-任务信息)[]  任务信息数组
- [Status](#status-标准错误码定义)    错误状态

示例

```lua
tasks, err = task.GetTasks({MaterialID = 100})
```

##### GetSchedulerOrderIndex 获取调度系统任务索引

```lua
task.GetSchedulerOrderIndex()
```

获取MPS任务绑定的调度系统任务索引

返回值

- int  任务索引
- [Status](#status-标准错误码定义) 错误状态

##### SetSchedulerOrderIndex 绑定调度系统任务索引

```lua
task.SetSchedulerOrderIndex(taskID, index)
```

将调度系统任务与MPS任务进行绑定，该指令在s消息处理脚本中调用，默认s消息处理脚本为DoSMessage.lua,但MPS接收到阶段为1的s消息时，需该指令，将两者任务之间进行绑定。

MPS接收到s消息后，需要根据调度系统任务编号找到对应的MPS系统任务，未绑定调度系统任务之前，传入DoSMessage.lua的taskID为0，这时候的DoSMessage.lua也就无法调用task模块的指令，只有在绑定成功后才能调用task模块的指令。

参数

- taskID  int  MPS任务编号
- index  int  调度系统任务编号

返回值

- [Status](#status-标准错误码定义) 错误状态

```lua
-- 阶段1，调度系统已启动任务
function BindSchedulerOrder(index, ikey, phase)
  if index <= 0 then
    return false
  end

  local err = task.SetSchedulerOrderIndex(ikey, index)
  if err:IsFailed() then
    return false
  end

  scheduler.SendMMessage(index, 1, 18, {phase})
  return true
end
```

##### SetArgv 设置任务局部参数

```lua
task.SetArgv(arg, argv)
```

设置任务的局部参数，不需要指定任务ID，系统会自动获取当前任务ID，并设置。

参数

- arg  string  任务局部参数名称，只能为字符串
- argv  string|number|bool  局部参数值，支持lua的三种类型，分别为字符串、数字(包括整型和浮点型)，布尔类型

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
-- 设置字符串参数
err = task.SetArgv("cardNum", "粤LC920H")
if err:IsFailed() then
  --  设置参数失败，进行错误处理
end

--  自动返回字符串类型变量cardNum
cardNum, err = task.GetArgv("cardNum")
if err:IsOK() then
  log.Info(cardNum)
end
-- 设置整数参数
err = task.SetArgv("entranceID", 1)
if err:IsFailed() then
  --  设置出入口编号参数失败，进行错误处理
end

--  自动返回整数类型变量entranceID
entranceID, err = task.GetArgv("entranceID")
if err:IsOK() then
  log.Info(entranceID)
end
-- 设置bool参数
err = task.SetArgv("isFinished", true)
if err:IsFailed() then
  --  设置出入口编号参数失败，进行错误处理
end

--  自动返回bool类型变量isFinished
isFinished, err = task.GetArgv("isFinished")
if err:IsOK() then
  log.Info(isFinished)
end
```

##### GetArgv 获取任务局部参数

```lua
task.GetArgv(arg)
```

获取任务局部参数，其值必须先前被SetArgv指令设置过，否则获取失败，返回值的类型由设置时的类型决定，即如果设置时是字符串，则获取时也是字符串，设置是数字，获取时也是数字，设置是bool，获取时也是bool。

参数

- arg  string  任务局部参数名称

返回值

- string|number|bool  具体类型由设置时的类型决定。

- [Status](#status-标准错误码定义) 错误状态

##### 设置特定类型任务局部参数

```lua
task.SetIntArgv(arg, argv)    -- 设置整数类型任务局部参数
task.SetFloatArgv (arg, argv) -- 设置浮点型任务局部参数
task.SetBoolArgv(arg, argv)   -- 设置布尔型任务局部参数
task.SetStringArgv(arg, argv) -- 设置字符串型任务局部参数
```

设置 整数/浮点/布尔/字符串 类型任务局部参数

参数

- arg  string  任务局部参数名称，只能为字符串
- argv  number/number/bool/string  整数/浮点/布尔/字符串 类型局部参数值

返回值

- [Status](#status-标准错误码定义) 错误状态

##### 获取特定类型任务局部参数

```lua
task.GetIntArgv(arg)     -- 获取整数类型任务局部参数
task.GetFloatArgv(arg)   -- 获取浮点型任务局部参数
task.GetBoolArgv(arg)    -- 获取布尔型任务局部参数
task.GetStringArgv(arg)  -- 获取字符串型任务局部参数
```

获取 整数/浮点/布尔/字符串 类型任务局部参数

参数

- arg  string  任务局部参数名称，只能为字符串

返回值

- number/number/bool/string  整数/浮点/布尔/字符串 类型局部参数值

- [Status](#status-标准错误码定义) 错误状态

##### SetFetchStation 设置装货站台

```lua
task.SetFetchStation(fetchStn)
```

参数

- fetchStn  int  装货站台ID

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local err = task.SetFetchStation(rLot.StationID)
```

##### GetFetchStation 获取装货站台

```lua
task.GetFetchStation()
```

获取使用task.SetFetchStation(fetchStn)设置的装货站台

返回值

- int    返回装货站台

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local fetchStn, err = task.GetFetchStation()
```

##### SetDelivStation 设置卸货站台

```lua
task.SetDelivStation(delivStn)
```

参数

- delivStn  int  卸货站台ID

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local err = task.SetDelivStation(deliverStn)
```

##### GetDelivStation 获取卸货站台

```lua
task.GetDelivStation()
```

获取使用task.SetDelivStation(delivStn)设置的卸货站台

返回值

- int    返回卸货站台
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local deliverStn, err = task.GetDelivStation()
```

##### SetDescription 设置任务描述

```lua
task.SetDescription(desc)
```

任务添加任务描述，一般用于记录当前任务的状态详情，方便查看任务已文本的形式快速明了任务情况。

注意，多次调用该指令，会覆盖以前的设置，以最近一次调用结果为准。

参数

- desc  string  任务描述

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
 task.SetDescription("start to take car")
```

##### SetMaterialID 绑定任务的物料

```lua
task.SetMaterialID(materialID)
```

参数

- materialID  int64  物料ID

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local err = task.SetMaterialID(materialID)
```

##### GetMaterialID 获取任务绑定的物料

```lua
task.GetMaterialID()
```

获取任务使用task.SetMaterialID(materialID)绑定的物料ID

返回值

- int64   与任务绑定的物料ID
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local materialID, err = task.GetMaterialID()
```

##### SetPhase 设置任务阶段

```lua
task.SetPhase(phase)
```

设置任务阶段。任务阶段一般与调度系统任务相关，一般将任务按照调度系统的任务划分阶段，调度系统每完成一个阶段，调用一次该指令，直到任务结束。

当然，其阶段也可以不与调度系统任务相关，具体请根据实际情况决定。

参数

- phase  int  阶段编号

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
task.SetPhase(common.OrderPhase.Fetch)
```

##### GetPhase 获取任务阶段

```lua
task.GetPhase()
```

获取使用task.SetPhase(phase)设置的任务阶段

返回值

- int    当前的任务阶段
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local phase, err = task.GetPhase()
```

##### SetType 设置任务类型

```lua
task.SetType(type)
```

参数

- type  int  任务类型

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local err = task.SetType(orderType)
```

##### GetType 获取任务类型

```lua
task.GetType()
```

获取使用task.SetType(type)设置的任务阶段

返回值

- int    任务类型
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local orderType, err = task.GetType()
```

##### SetEntrance 设置出入口

```lua
task.SetEntrance(entranceID)
```

设置任务关联的出入口，一般出入口是取货或者卸货的地方

参数

- entranceID  int  出入口编号

返回值

- [Status](#status-标准错误码定义) 错误状态

##### GetEntrance 获取出入口

```lua
task.GetEntrance()
```

获取使用task.SetEntrance(entranceID)设置的出入口

返回值

- int    出入口
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local entranceID, err = task.GetEntrance()
if err:IsFailed() then
  task.SetDescription("read entrancestatus1 failed")
  return false
end
```

##### SetRackID 绑定货架

```lua
task.SetRackID(rackID)
```

设置任务关联的货架

参数

- rackID  int  货架ID

返回值

- [Status](#status-标准错误码定义) 错误状态

##### GetRackID 获取货架

```lua
task.GetRackID()
```

获取使用task.SetRackID(rackID)绑定的货架ID

返回值

- int    货架ID

- [Status](#status-标准错误码定义) 错误状态

##### SetGP 设置全局参数值

```lua
task.SetGP(arg, argv)
```

设置全局参数值，与具体任务无关

参数

- arg  string  全局参数名称，只能为字符串
- argv  string|number|bool  全局参数值，支持lua的三种类型，分别为字符串、数字(包括整型和浮点型)，布尔类型

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local err = task.SetGP("AgvStation", station)
```

##### GetGP 获取全局参数值

```lua
task.GetGP(arg)
```

参数

- arg  string  全局参数名称，只能为字符串

返回值

- string|number|bool  全局参数的值，类型与使用task.SetGP(arg, argv)设置时的一致
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
local agvFloor, err = task.GetGP("AgvFloor")
if err:IsFailed() then
  return false, false
end
```

##### GetChannel 获取任务绑定的通道

```lua
task.GetChannel(channel)
```

一个任务脚本启动时，会预先绑定名称为ExitCh、SMessageCh的两个channel对象，用户可以通过该指令取回对象，并向其发送消息。

channel对象通过send发送数据，通过channel.select函数进行数据接受监听，具体用法[参考](https://github.com/yuin/gopher-lua)，其通常用在从DoSMessage.lua脚本向其他脚本传递接收到的s消息参数。

参数

- channel  string  channel名称

返回值

- channel  lua.LChannel类型

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
--  阶段2、4、6、8、10 进入该函数，向关联任务发送s消息数据
function DoPhase(index, phase, magic2, ikey)
  local orderPhase, err = task.GetPhase()
  if err:IsFailed() then
    return false
  end

  if orderPhase < phase then
    SMessageCh:send({index, phase, magic2, ikey})
    return true
  end

  scheduler.SendMMessage(index, 1, 18, {phase})
  return false
end
```

##### NextID 获取可用的任务编号

```lua
task.NextID()
```

获取可用的任务编号，用于新任务，规则是当前任务列表中任务编号最大值+1

返回值

- int    返回新的ID

- [Status](#status-标准错误码定义) 错误状态

##### SetPriority 设置任务优先级

```lua
task.SetPriority(priority, taskID)
```

获取可用的任务编号，用于新任务

参数

- priority  int  优先级
- taskID  int  待设置优先级的任务号，可选参数

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
err = task.SetPriority(10)
if err:IsOK() then
  print("将当前任务优先级设置为10")
end

err = task.SetPriority(12, 100)
if err:IsOK() then
  print("将100号任务优先级设置为12")
end
```

##### GetPriority 获取任务优先级

```lua
task.GetPriority(taskID)
```

参数

- taskID  int  要获取优先级的任务号，可选参数，但不填写时，表示获取当前任务的优先级

返回值

- int    任务优先级

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
prio, err = task.GetPriority()
if err.IsOK() then
  print("当前任务优先级为: ", prio)
end

prio, err = task.GetPriority(100)
if err.IsOK() then
  print("100号任务优先级为: ", prio)
end
```

##### SetAgvID 设置任务AgvID

```lua
task.SetAgvID(agvID)
```

记录执行该任务所使用的AGV

参数

- agvID  int  Agv ID

返回值

- [Status](#status-标准错误码定义) 错误状态

示例

```lua
-- 阶段4,调度系统询问MPS系统，是否允许装货
function DoFetch(index, phase, magic2, ikey, carrierNo)
  task.SetAgvID(carrierNo)
  task.SetPhase(common.OrderPhase.Fetch)
  scheduler.SendMMessage(index, 1, 18, {phase})
  return true
end
```

##### GetAgvs 获取全部AGV即时状态

```lua
task.GetAgvs()
```

返回值

- [Agvs](#agv-状态信息)[] Agv状态数组
- [Status](#status-标准错误码定义) 错误状态

示例

```lua
-- 通过 AgvID, 获取Agv所在的X,Y，当只有一台Agv时，agvID可以直接给当前agvid
function agvStation.GetAgvStation(agvID)
  local station, x, y = 0, 0, 0
  local agvs, err = task.GetAgvs()

  if err:IsFailed() then
    x1print("err:" .. err:String())
  else
    -- 获取指定agvID所在的楼层
    for _, agv in pairs(agvs) do
      if agvID == 0 or agvID == agv.AgvID then
        x, y = agv.X, agv.Y
        station = getStationByPos(agv.X, agv.Y)
      end
    end
  end

  -- 设置全局AGV所在楼层状态
  err = task.SetGP("AgvStation", station)
  if err:IsFailed() then
    x1print("Set AgvStation failed, err: " .. err:String())
    station = false
  end

  -- 返回是否在出入口
  return station, true
end
```

##### GetTaskTime 获取指定站台之间任务所需时间

```lua
task.GetTaskTime(originStation, targetStation)
```

获取指定站台之间任务所需时间，主要用于优化Lua代码

参数

- originStation  number  起始站台
- targetStation  number  目标站台

返回值

- number 返回耗时，由task.UpdateTaskTime()所设置

- [Status](#status-标准错误码定义) 错误状态

##### UpdateTaskTime 更新指定站台之间任务所消耗的时间

```lua
task.UpdateTaskTime(originStation, targetStation, time)
```

更新指定站台之间任务所消耗的时间，主要用于优化Lua代码

参数

- originStation  number  起始站台
- targetStation  number  目标站台

返回值

- [Status](#status-标准错误码定义) 错误状态

##### GetBindAgvTime 获取Agv任务绑定时间

```lua
task.GetBindAgvTime()
```

返回值

- number 返回Agv任务绑定时间，由task.SetBindAgvTime ()所设置；未设置时为0

  UTC 时间戳格式，单位秒

- [Status](#status-标准错误码定义) 错误状态

##### SetBindAgvTime 设置Agv任务绑定时间

```lua
task.SetBindAgvTime(timestamp)
```

设置当前任务的Agv任务绑定时间(Agv任务开始时间)

参数

- timestamp  number  设置Agv任务绑定时间，UTC 时间戳格式，单位秒

返回值

- [Status](#status-标准错误码定义) 错误状态

##### GetDeliveryTime 获取Agv卸货时间

```lua
task.GetDeliveryTime()
```

获取当前任务的Agv卸货时间（Agv任务完成时间）

返回值

- number  返回Agv卸货时间，由task.SetDeliveryTime ()所设置；未设置时为0
  UTC 时间戳格式，单位秒

- [Status](#status-标准错误码定义) 错误状态

##### SetDeliveryTime 设置Agv卸货时间

```lua
task.SetDeliveryTime(timestamp)
```

设置当前任务的Agv卸货时间(Agv任务完成时间)

参数

- timestamp  number  设置Agv卸货时间，UTC 时间戳格式，单位秒

返回值

- [Status](#status-标准错误码定义) 错误状态

### util 工具

#### 工具相关数据类型

##### HttpRequest 请求信息

```go
type HttpRequest struct {
  URL         string // URL
  ContentType string // 编码类型，可选，默认为："application/json;charset=UTF-8"
  Body        string // 请求内容，可选
}
```

##### HttpRespone 响应信息

```go
type HttpRespone struct {
  ContentType string  // 编码类型
  Body        string  // 响应内容
}
```

#### 工具指令

##### Sleep 休眠

```lua
util.Sleep(duration)
```

休眠时间精度为1ms,为防止脚本中指令被滥用，最小休眠时间为100ms，即0.1s，参数支持浮点数。取值从0.1~∞

参数

- duration  double  休眠时间（秒为单位）

示例

```lua
-- Sleep 500 毫秒
util.Sleep(0.5)

-- Sleep 秒
util.Sleep(8)
```

##### ParseTime 时间字符串转UTC时间

参数

- time  string  2006-01-02 15:04:05 格式的时间字符串

返回值

- int  成功：返回Unix时间戳，单位秒；出错：返回0

##### Lock/Unlock 加锁/解锁

```lua
util.Lock(id, ttl)  -- 加锁
util.Unlock(id)     -- 解锁
```

用于对代码片段进行加锁，锁超时时间为可选参数，如果不给，则默认为0，即不设置超时时间，直到主动释放

参数

- id  string  锁名称
- ttl  int64  锁超时时间，可选参数，单位毫秒

返回值

- [Status](#status-标准错误码定义)      错误状态

示例

```lua
-- 加锁
err = util.Lock(common.LockParkTake, 10000)
if err:IsFailed() then
  return false, "util.Lock failed", 0
end

-- 解锁
util.Unlock(common.LockParkTake)
```

##### HttpPost 发送POST请求

```lua
util.HttpPost(req)
```

参数

- req  [HttpRequest](#httprequest-请求信息)  HTTP请求参数

返回值

- [HttpRespone](#httprespone-响应信息)   HTTP 响应
- [Status](#status-标准错误码定义)      错误状态

示例

```lua
local request = {
  URL = cfg.HttpHost .. clouds.Command.GET_PAYMENT_URL_ADDR,
  ContentType = "application/json;charset=UTF-8",
  Body = getPaymentUrl(clouds.Command.GET_PAYMENT_URL, cfg, carInfo)
}

local response, err = util.HttpPost(request)
if err:IsFailed() then
  log.Errorf("GetPaymentUrl(%s) failed, error: %s", carInfo.Properties.ParkTag, err:String())
  return nil, false, err
else
  log.Errorf("GetPaymentUrl(%s) succeed, respone: %s", carInfo.Properties.ParkTag, response)
end
```

##### HttpGet 发送GET请求

```lua
util.HttpGet(req)
```

参数

- req  [HttpRequest](#httprequest-请求信息)  HTTP请求参数

返回值

- [HttpRespone](#httprespone-响应信息)   HTTP 响应
- [Status](#status-标准错误码定义)      错误状态

##### MD5 计算md5

```lua
util.MD5(val)
```

参数

- val  string   字符串

返回值

- string   md5信息

示例

```lua
local sign = util.MD5(string.format("%s%d%d", cfg.Private, ts, cmd)),
```

## 第三方指令

### json 指令

#### 支持的数据类型

| Lua    | JSON                                                         |
| ------ | ------------------------------------------------------------ |
| nil    | null                                                         |
| number | number                                                       |
| string | string                                                       |
| table  | object: when table is non-empty and has only string keys<br />array: when table is empty, or has only sequential numeric keys starting from 1 |

#### encode 编码

```lua
json.encode(val)
```

编码一个lua值为json字符串，失败时返回两个值，成功时返回一个值

参数

- val  lua变量

返回值

- string   成功时为json字符串；失败时为nil

- string  编码失败时，为错误信息

#### decode 解码

```lua
json.decode(str)
```

解码一个JSON字符串，失败时返回两个值，成功时返回一个值

参数

- str  json字符串

返回值

- val   成功时为lua变量；失败时为nil

- string  编码失败时，为错误信息

示例

```lua
-- 参考：https://github.com/layeh/gopher-json/json_test.go
local json = require("json")
assert(type(json) == "table")
assert(type(json.decode) == "function")
assert(type(json.encode) == "function")

assert(json.encode(true) == "true")
assert(json.encode(1) == "1")
assert(json.encode(-10) == "-10")
assert(json.encode(nil) == "null")
assert(json.encode({}) == "[]")
assert(json.encode({1, 2, 3}) == "[1,2,3]")

local _, err = json.encode({1, 2, [10] = 3})
assert(string.find(err, "sparse array"))

local _, err = json.encode({1, 2, 3, name = "Tim"})
assert(string.find(err, "mixed or invalid key types"))

local _, err = json.encode({name = "Tim", [false] = 123})
assert(string.find(err, "mixed or invalid key types"))

local obj = {"a", 1, "b", 2, "c", 3}
local jsonStr = json.encode(obj)
local jsonObj = json.decode(jsonStr)
for i = 1, #obj do
  assert(obj[i] == jsonObj[i])
end

local obj = {name = "Tim", number = 12345}
local jsonStr = json.encode(obj)
local jsonObj = json.decode(jsonStr)
assert(obj.name == jsonObj.name)
assert(obj.number == jsonObj.number)
assert(json.decode("null") == nil)

assert(json.decode(json.encode({person = {name = "tim"}})).person.name == "tim")

local obj = {abc = 123,def = nil}
local obj2 = {obj = obj}
obj.obj2 = obj2
assert(json.encode(obj) == nil)

local a = {}
for i = 1, 5 do
  a[i] = i
end
assert(json.encode(a) == "[1,2,3,4,5]")
```

## 服务地址

### 相关地址

| 名称                                                         | 地址                                                         | 说明                           |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------ |
| MPS管理系统                                                  | http://172.16.1.201:9999/                                    | Web                            |
| RabbitMQ                                                     | [http://172.16.1.201:15672](http://172.16.1.201:15672/#/users)/ | Web，RabbitMQ管理              |
| Code-Server                                                  | http://172.16.1.201:9876/                                    | Web                            |
| Docker容器                                                   | http://172.16.1.201:9000/                                    | Web                            |
| InfluxDB                                                     | http://172.16.1.201:8086/                                    | Web                            |
| [PostgreSQL](https://www.postgresql.org/)                    | tcp://172.16.1.201:5432                                      | [DBeaver](https://dbeaver.io/) |
| [Micro](https://github.com/micro/micro/releases/download/v2.9.3/) Web网关 | 启动：micro --registry=etcd web # In VMmicro --registry=etcd --registry_address=[172.16.1.201:2379](http://172.16.1.201:8086/) web  # Out VMdocker-compose exec mps-micro-gateway /micro --registry=etcd web查看：[http://172.16.1.201:8082/](http://172.16.1.201:8086/) |                                |
| agvs                                                         | http://172.16.1.201:8080/agvs/graphql                        | [GraphQL](https://graphql.cn/) |
| areas                                                        | http://172.16.1.201:8080/areas/graphql                       | [GraphQL](https://graphql.cn/) |
| camera                                                       | http://172.16.1.201:8080/camera/graphql                      | [GraphQL](https://graphql.cn/) |
| config                                                       | http://172.16.1.201:8080/config/graphql                      | [GraphQL](https://graphql.cn/) |
| ledscreen                                                    | http://172.16.1.201:8080/ledscreen/graphql                   | [GraphQL](https://graphql.cn/) |
| log                                                          | http://172.16.1.201:8080/log/graphql                         | [GraphQL](https://graphql.cn/) |
| lpr                                                          | http://172.16.1.201:8080/lpr/graphql                         | [GraphQL](https://graphql.cn/) |
| map                                                          | http://172.16.1.201:8080/map/graphql                         | [GraphQL](https://graphql.cn/) |
| materials                                                    | http://172.16.1.201:8080/materials/graphql                   | [GraphQL](https://graphql.cn/) |
| notifications                                                | http://172.16.1.201:8080/notifications/graphql               | [GraphQL](https://graphql.cn/) |
| opc                                                          | http://172.16.1.201:8080/opc/graphql                         | [GraphQL](https://graphql.cn/) |
| payment                                                      | http://172.16.1.201:8080/payment/graphql                     | [GraphQL](https://graphql.cn/) |
| racks                                                        | http://172.16.1.201:8080/racks/graphql                       | [GraphQL](https://graphql.cn/) |
| racklots                                                     | http://172.16.1.201:8080/racklots/graphql                    | [GraphQL](https://graphql.cn/) |
| reports                                                      | http://172.16.1.201:8080/reports/graphql                     | [GraphQL](https://graphql.cn/) |
| tasks                                                        | http://172.16.1.201:8080/tasks/graphql                       | [GraphQL](https://graphql.cn/) |

说明：所有IP地址，如无特别说明，均指CentOS系统的IP地址。

### API 地址

| 服务          | URL                                                          | 功能                            |
| ------------- | ------------------------------------------------------------ | ------------------------------- |
| adv/广告机    | http://172.16.1.201:8080/adv<br />存取车：/ParkTakeCarManager/Operation<br />出入口状态：/IExportManager/Data<br />取车队列：/TaskManager/List<br />获取车牌号：/LicManager/Data<br />保存前端界面语言：/SaveLanguage<br />获取前面界面语言：/GetLanguage | 广告机操作由 **adv.lua** 响应   |
| guide/导引屏  | http://172.16.1.201:8080/guide/                              | 导引屏操作由 **guide.lua** 响应 |
| map/地图      | http://172.16.1.201:8080/map/layout/files                    | 上传点、段、站台文件            |
| opc/PLC       | http://172.16.1.201:8080/opc/item/file                       | 上传出入口配置文件              |
| user/用户管理 | http://172.16.1.201:8080/user/http://172.16.1.201:8080/users/ | 用户管理                        |

## 脚本约定功能

脚本约定功能的脚本名称（包括大小写），函数签名都是固定的，不能修改。

### adv.lua 广告机

#### 操作码

| 操作码            | 值   | 含义                             |
| ----------------- | ---- | -------------------------------- |
| PARK              | 1    | 取车确定操作完成后发送，确定存车 |
| TAKE              | 2    | 输完车牌号后发送，确定取车       |
| TEST_PARK         | 3    | 是否允许存车                     |
| TEST_TAKE         | 4    | 是否允许取车                     |
| TEST_UNCERTAIN    | 5    | 判断是存车还是取车               |
| PASSWORD_PARK     | 10   | 密码存车                         |
| PASSWORD_TAKE     | 11   | 密码取车                         |
| CLOUD_PARK        | 101  | 取车确定操作完成后发送，确定存车 |
| CLOUD_TAKE        | 102  | 输完车牌号后发送，确定取车       |
| CLOUD_TEST_PARK   | 103  | 是否允许存车                     |
| CLOUD_PAY_SUCCESS | 104  | 支付成功通知                     |

说明：操作码是与广告机约定而定义的。

#### Operation 广告机操作

```lua
function Operation(entranceID, code, parkingTag)
```

云平台的通知，也会转发到Operation()，

参数

- entranceID  int 出生口编号

- code  int [操作码](#操作码)

- parkingTag  string  停车标识，

返回值

- string  json字符串

#### EntranceStatus 出入口状态

```lua
function EntranceStatus(entranceID, code)
```

返回出入口状态给广告机

参数

- entranceID  int 出生口编号
- code  int 操作码（暂时未使用）

返回值

- string  json字符串

### guide.lua 导引屏

#### 导引屏状态码

| 名称              | 值   | 含义                      |
| ----------------- | ---- | ------------------------- |
| NORMAL            | 0    | 欢迎使用AGV停车库         |
| FORWARD           | 1    | 请向前行驶                |
| BACKARD           | 2    | 请后退                    |
| LEFT              | 3    | 请向左行驶                |
| RIGHT             | 4    | 请向右行驶                |
| GO_OUT            | 5    | 车已取至出入口，请驶出    |
| NO_PARKING_LOT    | 6    | 没有合适的车位，请驶出    |
| ELEVATOR_ABNORMAL | 7    | 升降机异常                |
|                   | 8    |                           |
| OVER_HEIGHT       | 9    | 进车车辆超高,请退出       |
| OVER_LENGTH       | 10   | 进车超长,请退出           |
| OVER_WEIGHT       | 11   | 进车车辆超重,请退出       |
| OVER_WIDTH        | 12   | 进车车辆超宽,请退出       |
| LANE_DEPARTURE    | 13   | 车道偏离                  |
| PARKING_NORMAL    | 14   | 车已停好                  |
| BAD_NETWORK       | 15   | 网络信号异常              |
| MANUAL_MODE       | 16   | 手动模式(维护模式,维护中) |

说明：状态码是与导引屏约定而定义的。

#### ParkGuideProcess 导引屏状态

```lua
function ParkGuideProcess(entranceID)
```

参数

- entranceID  int 出生口编号

返回值

- string  json字符串

### button-agv.lua Agv操作

主界面

![急停与恢复](img\main-agv.png)

AGV管理列表

![急停与恢复2](img\agv-mgr-list.png)

#### Stop/Recovery 急停/恢复

```lua
function Stop(agvID)    -- 急停
function Recovery(agvID)-- 恢复
```

参数

- agvID int AgvID

返回值

- bool  操作是否成功

### button-emergencyStop.lua 全部急停

主界面急停

![全部急停](img\emgr-stop-all.png)

#### EmergencyStop 全部急停

```lua
function EmergencyStop()
```

返回值

- bool  操作是否成功

### button-material.lua 物料操作

物料管理/车辆管理

![物料管理](img\material-op.png)

#### GoOut/GoIn 出/入库

```lua
function GoOut(materialID, entranceID)  -- 出库
function GoIn(materialID, entranceID)   -- 入库
```

参数

- materialID  int64 物料ID

- entranceID  int 出入口编号

返回值

- bool  操作是否成功

### button-movePallet.lua 简易任务

简易任务，用于移库操作

![简易任务](img\main-simple-task.png)

#### button-monitoring.lua 主界面操作

主界面反存和出库

![反存与出库](img\main-op.png)

#### GoIn/GoOut 反存/出库

```lua
function GoIn(cellID)   -- 反存
function GoOut(cellID)  -- 出库
```

参数

- cellID  int 储位ID

返回值

- bool  操作是否成功

### doSMessage.lua 调度系统消息

#### DoSMessage s消息处理

```lua
function DoSMessage(index, phase, magic2, ikey, carrierNo, tsID)
```

参数

- index uint16  AGV管理系统中的任务索引号

- phase uint16  任务阶段

- magic2  uint16  用户自定义

- ikey  uint16  任务ID

- carrierNo uint8 车辆ID，0 – 无效值

- tsID  int8  AGV管理系统中正在执行的任务脚本程序ID，有效值为1~255

返回值

- bool  操作是否成功

### systemGo.lua 系统任务

系统任务脚本，相当于主循环，用于执行周期性检测任务。

![SystemGo](img\task-systemgo.png)

## 工具脚本

### 控制台打印脚本

初始版本在 [青岛海信](https://codeup.aliyun.com/yfrobots/projects/qdhx_script.git) 脚本中的 xprint.lua，优化后的版本在 [方舟测试系统](git@codeup.aliyun.com:yfrobots/projects/yfVisit-scripts.git) 中，最新版本支持：

- 打印所有lua数据类型
- 打印脚本文件名、行号、函数名称，时间

```lua
-- ==================================================================
-- 功能：
--    1、控制台日志打印，支持Table，以简化使用 print，减少出错
--    2、可以自动打印函数名
-- 作者：龙武全
-- 日期：2021/8/10 
-- 更新记录：
-- 1、2021/8/10 初始化，参考 https://blog.csdn.net/weixin_33877885/article/details/85646976
-- ==================================================================
local debug = require("debug")

-- nLevel: 
--   0: 不打印函数名; 
--   1: 是 xprint，一般不用
--   >=2: 依次打印上 nLevel 级别的函数名称
function xnprint(nLevel, ...)
  local tv = "" -- 初始值，可以加比如："\n"
  local kTAB = " " -- Tab 使用什么显示
  local xn = 0 -- 当前缩进

  if nLevel > 0 then
    local callFunc = debug.getinfo(nLevel)
    local file = string.gsub(callFunc.source, "//", "/")
    local date = os.date("%X ")
    
    -- 将“.///scripts/strategy.lua” =》 “./scripts/strategy.lua”
    file = string.gsub(file, "//", "/") 
    tv = date .. file .. ":L" .. callFunc.currentline .. " " .. callFunc.name .. "(): "
  end

  local function tvlinet(xn)
    for i = 1, xn do
      tv = tv .. kTAB
    end
  end

  local function printTab(i, v)
    if type(v) == "table" then
      tvlinet(xn)
      xn = xn + 1
      tv = tv .. i .. ":Table{\n"
      for j, val in pairs(v) do
        printTab(j, val)
      end

      tvlinet(xn)
      tv = tv .. "}\n"
      xn = xn - 1
    elseif type(v) == nil then
      tvlinet(xn)
      tv = tv .. i .. ":nil\n"
    else
      tvlinet(xn)
      tv = tv .. i .. ":" .. tostring(v) .. "\n"
    end
  end

  local function dumpParam(tab)
    for i = 1, #tab do
      if tab[i] == nil then
        tv = tv .. "nil" .. kTAB
      elseif type(tab[i]) == "table" then
        xn = xn + 1
        tv = tv .. "\ntable{\n"
        for j, val in pairs(tab[i]) do
          printTab(j, val)
        end
        tv = tv .. kTAB .. "}\n"
      else
        tv = tv .. tostring(tab[i]) .. kTAB
      end
    end
  end

  local x = ...
  if type(x) == "table" then
    for j, val in pairs(x) do
      printTab(j, val)
    end
  else
    dumpParam({...})
  end

  -- 去掉最后一个换行符
  tv = string.sub(tv, 1, -2)

  print(tv)
end

-- 不打印函数名称
function x0print(...)
  xnprint(0, ...)
end

-- 打印主调函数名称
function x1print(...)
  xnprint(3, ...) -- 因为要加上 x1print/xnprint
end

-- 打印主调函数上一级的函数名称
function x2print(...)
  xnprint(4, ...) -- 因为要加上 x2print/xnprint
end
```

### 云平台功能对接脚本

云平台对接脚本，初始版本在[青岛海信](https://codeup.aliyun.com/yfrobots/projects/qdhx_script.git) 脚本中的 clouds.lua，支付功能的版本在[惠州冠昌雅居](git@codeup.aliyun.com:yfrobots/projects/hzgcyj-scripts.git)中

### 自动化出入库测试脚本

自动化出入库测试脚本，初始版本在[方舟III](git@codeup.aliyun.com:yfrobots/projects/yfVisit-scripts.git) 脚本中的 auto_test.lua

## REST Client 脚本

[Rest Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)脚本，可以直接在[VSCode](https://code.visualstudio.com/)中执行，需要先安装插件。

- [adv.rest](https://codeup.aliyun.com/yfrobots/mps/adv-service/blob/develop/adv-api/adv.rest) 广告机

- [cloud.rest](https://codeup.aliyun.com/yfrobots/mps/cloud-service/blob/develop/cloud-api/cloud.rest) 云平台

- [task.rest](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-api/task.rest) 任务

## 在线联调脚本

参考

- [使用Code-Server在线联调Lua](https://alidocs.dingtalk.com/i/team/nb9XJll6ll2llGyA/docs/nb9XJkkPDE44PmyA?corpId=ding72f559c855c56bd935c2f4657eb6378f# 「使用Code-Server在线联调Lua」)
- [在VSCode中使用Remote-SSH](https://alidocs.dingtalk.com/i/team/nb9XJll6ll2llGyA/docs/nb9XJkkPDEbYDmyA?corpId=ding72f559c855c56bd935c2f4657eb6378f# 「在VSCode中使用Remote-SSH」)

### 使用VSCode在线联调

- 安装[VSCode](https://az764295.vo.msecnd.net/stable/507ce72a4466fbb27b715c3722558bb15afa9f48/VSCodeUserSetup-x64-1.57.1.exe)后，再安装相关插件：
  - [EmmyLua](https://ci.appveyor.com/project/EmmyLua/vscode-emmylua/build/artifacts)，版本>=0.3.95
  - [Lua Helper](https://marketplace.visualstudio.com/items?itemName=yinfei.luahelper), lua工具
  - [Remote-SSH]([Remote - SSH - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh))，远程连接到CentOS
  - [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)，管理Docker镜像
  - [Rest Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)，执行Restful、GraphQL API
- 在CentOS中安装[Oracle JDK11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)，或者安装OpenJDK11

> EmmyLua <=0.3.95，可以使用JDK8；但后缀版本，需要JDK11。
>
> JDK11 如果提示没有JRE，可运行下面命令：
>
> bin/jlink --module-path jmods --add-modules java.desktop --output jre

```sh
# 安装OpenJDK11
sudo yum -y install java-11-openjdk
```

- 使用VSCode的[Remote-SSH](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh)连接CentOS，并打开 ~/mps/mps-docker-compose/mps-data/task-service(不要打开下面的 scripts目录，因为脚本中使用了相对它的目录)
- 其它操作，就与在Code-Server中一样了

- 使用[task.rest](https://codeup.aliyun.com/yfrobots/mps/task-service/blob/develop/task-api/task.rest) 任务脚本启动Lua调试任务，和启动测试任务等

![LuaDebug](img\lua-debug.png)
